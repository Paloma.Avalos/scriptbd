const { User } = require('../models/usuario.js');

class TrabajadorDb {

    static async obtenerTrabajadorPorNombre(nombre_completo){
        try {
            return await User.find({ 'nombre_completo': new RegExp(nombre_completo, "i") }).lean();
        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }

    static async listarTodosTrabajadores(limit, offset, params) {

        let query = [];

        const myCustomLabels = {
            totalDocs: 'count',
            docs: 'results'
        };

        const options = {
            sort: { 'resultado_sintomatologico.orden': -1, 'numero_sintomas': -1, 'fecha_ultimo_test': -1},
            offset: offset,
            limit: limit,
            lean: true,
            leanWithId: false,
            customLabels: myCustomLabels
        };

        /**
         * Filtros de consulta
         */
        let queryFilter = [];
        let fnAddFilter = (field, value) => {
            if (queryFilter.length == 0) {
                queryFilter.push({ $match: { $and: [] } });
            }
            queryFilter[0].$match.$and.push({
                [field]: value
            });
        };

        // Preparar la data antes de aplicar los filtros
        query = [{
            $unwind: {
                path: '$resultado_medico',
                preserveNullAndEmptyArrays: true
            }
        }, {
            $sort: {
                'resultado_medico.fecha_registro': 1
            }
        }, {
            $group: {
                _id: '$_id',
                numero_documento: {
                    $first: '$numero_documento'
                },
                apellido_paterno: {
                    $first: '$apellido_paterno'
                },
                apellido_materno: {
                    $first: '$apellido_materno'
                },
                nombres: {
                    $first: '$nombres'
                },
                datos_compania: {
                    $first: '$datos_compania'
                },
                resultado_evaluacion: {
                    $first: '$resultado_evaluacion'
                },
                resultado_sintomatologico: {
                    $first: '$resultado_sintomatologico'
                },
                resultado_medico: {
                    $push: '$resultado_medico'
                },
                fecha_ultimo_test: {
                    $first: '$fecha_ultimo_test'
                },
                numero_sintomas: {
                    $first: '$numero_sintomas'
                },
                activo: {
                    $first: '$activo'
                },
                cargo:{
                    $first: '$cargo'
                },
                nombre_completo: {
                    $first: '$nombre_completo'
                },
            }
        }, {
            $addFields: {
                resultado_medico: {
                    $arrayElemAt: [
                        '$resultado_medico', -1
                    ]
                }
            }
        }];

        if (params.hasOwnProperty("sede") && parseInt(params.sede)) {
            fnAddFilter('datos_compania.id_sede', parseInt(params.sede));
        }
        if (params.hasOwnProperty("unidad") && parseInt(params.unidad)) {
            fnAddFilter('datos_compania.id_unidad', parseInt(params.unidad));
        }
        if (params.hasOwnProperty("situacion") && parseInt(params.situacion)) {
            fnAddFilter('resultado_medico.id_condicion', params.situacion);
        }
        if (params.hasOwnProperty("sintomatologia") && params.sintomatologia) {
            fnAddFilter('resultado_evaluacion.llave', params.sintomatologia);
        }
        if (params.hasOwnProperty("clasificacion") && params.clasificacion) {
            fnAddFilter('resultado_sintomatologico.llave', params.clasificacion);
        }

        query.push(...queryFilter);

        if (params.hasOwnProperty("busqueda") && params.busqueda) {
            console.log("busqueda:", params.busqueda);
            query.push({
                $match: {
                    $or: [
                        { nombres: { $regex: params.busqueda, $options: "i" } },
                        { apellido_paterno: { $regex: params.busqueda, $options: "i" } },
                        { apellido_materno: { $regex: params.busqueda, $options: "i" } },
                        { numero_documento: { $regex: params.busqueda, $options: "i" } },
                        { nombre_completo: { $regex: params.busqueda, $options: "i" } },
                    ]

                },
            });
        }

        query.push({
            $match: { activo: true },
        });

        query.push({
            $project: {
                numero_documento: 1,
                apellido_paterno: 1,
                apellido_materno: 1,
                nombres: 1,
                'datos_compania.id_unidad': 1,
                'datos_compania.unidad': 1,
                'datos_compania.id_sede': 1,
                'datos_compania.sede': 1,
                resultado_evaluacion: 1,
                resultado_sintomatologico: 1,
                resultado_medico: 1,
                fecha_ultimo_test: 1,
                numero_sintomas: 1,
                cargo: 1,
                nombre_completo: 1
            }
        });

        console.log("query:", JSON.stringify(query));

        try {

            let trabajadores = await User.aggregatePaginate(User.aggregate(query), options);
            console.log("lenght:", trabajadores.results.length);
            return trabajadores;
        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }

}

module.exports.TrabajadorDb = TrabajadorDb;
