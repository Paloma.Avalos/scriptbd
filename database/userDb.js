const {User} = require('../models/usuario');

class UserDb {

    static async obtenerUsuarioPorId(idTrabajador) {

        try {
            return await User.findById(idTrabajador).select("nombres  apellido_paterno apellido_materno id_tipo_documento tipo_documento numero_documento telefono id_riesgo_exposicion riesgo_exposicion id_tipo_colaborador tipo_colaborador datos_compania correo_electronico  activo  datos_usuario.nombre_usuario fecha_creacion");
        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }

    static async obtenerTodosUsuario(documento, estado, tipoUsuario, limite, pagina) {

        try {
            // return await User.find({
            //     numero_documento: documento,
            // }).select("nombres  apellido_paterno apellido_materno id_tipo_documento tipo_documento numero_documento telefono id_riesgo_exposicion riesgo_exposicion id_tipo_colaborador tipo_colaborador  datos_compania correo_electronico  activo datos_usuario.nombre_usuario fecha_creacion");

            let query = {};
            if (documento && documento.length) {
                query.numero_documento = documento;
            }
            if (tipoUsuario && tipoUsuario.length) {
                query.id_tipo_colaborador = parseInt(tipoUsuario);
            }
            if (estado && estado.length) {
                query.activo = parseInt(estado) ? true : false;
            }

            return await User.paginate(query, {
                page: pagina,
                limit: limite
            });

        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }

    static async obtenerTodosUsuarioCompleto(documento, estado, tipoUsuario) {

        try {

            let query = {};
            if (documento && documento.length) {
                query.numero_documento = documento;
            }
            if (tipoUsuario && tipoUsuario.length) {
                query.id_tipo_colaborador = parseInt(tipoUsuario);
            }
            if (estado && estado.length) {
                query.activo = parseInt(estado) ? true : false;
            }

            return await User.find(query);

        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }

    static async ObtenerUsuarioPorNombre(nombre_completo, limite, pagina){
        
        try {            
            const query = { 'nombre_completo': new RegExp(nombre_completo, "i")}
            return await User.paginate(query, {
                page: pagina,
                limit: limite
            });

        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }

    static async ObtenerUsuarioPorNombreCompleto(nombre_completo){
        
        try {            
            const query = { 'nombre_completo': new RegExp(nombre_completo, "i")}
            return await User.paginate(query, {
                page: pagina,
                limit: limite
            });

        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }
    static async obtenerUsuarioPorNroDoc(tipo_documento,numero_documento) {

        let tipoDocumento = tipo_documento  
        let nroDocumento = numero_documento;
    
        try {
            return await User.findOne({'tipo_documento': tipoDocumento, 'numero_documento': nroDocumento}).lean();
        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }


}

module.exports.UserDb = UserDb;
