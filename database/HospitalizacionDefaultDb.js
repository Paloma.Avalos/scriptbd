const { Hospitalizacion } = require('../models/hospitalizacion');
const { ResourceNotFoundError, InternalError } = require("../error/BusinessError");

class HospitalizacionDefaultDb {


    static async grabarHospitalizacion(Hospitalizacion) {

        try {
            return await Hospitalizacion.save();
        } catch (error) {
            throw new ResourceNotFoundError('Un error inesperado actualizarUserSave', error.messages || [error.message]);
        }
    }

    static async obtenerHospitalizacionPorId(idHospitalizacion) {

        try {
            return await Hospitalizacion.findById(idHospitalizacion).lean();
        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }

    static async obtenerHospitalizacionPorIdTrabajador(idTrabajador) {

        try {
            return await Hospitalizacion.find({ 'id_colaborador': idTrabajador }).lean();
        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }

    static async obtenerHospitalizacionPorColaborarCaso(request) {

        const id_colaborador = request.query.id_colaborador;
        const id_caso = request.query.id_caso;
        try {           
            if (request.query.id_colaborador!=='null') {
                return await Hospitalizacion.find({"id_colaborador" : id_colaborador}).lean();
            }
            else if(request.query.id_caso!=='null') {
                return await Hospitalizacion.find({"id_caso" : id_caso}).lean();
            }            
        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }    

    }

    static async actualizarHospitalizacion(idHospitalizacion,hospitalizacion ){
        try{
            return await Hospitalizacion.findByIdAndUpdate(idHospitalizacion,hospitalizacion);
        }catch(error){
            console.log('Error:', error);
            throw new ResourceNotFoundError('Un error inesperado HospitalizacionActualizar', error.messages || [error.message]);  
        }
    }

}

module.exports.HospitalizacionDefaultDb = HospitalizacionDefaultDb;