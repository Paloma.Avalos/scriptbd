const { Temperatura } = require('../models/temperatura');
const { ResourceNotFoundError } = require("../error/BusinessError");
const util = require('../util/util');

class TemperaturaDefaultDb {

    /**
     * Registrar documento Temperatura en BD
     * @param {object} temperatura - Documento Temperatura a registrar en BD
     * @return {object} Documento temperatura actualizaco post registro
     */
    static async guardarTemperatura(temperatura) {

        try {

            // Guardar y retornar documento temperatura actualziado
            return await temperatura.save();

        } catch (error) {

            // Lanzar error ante problema en el registro de temperatura
            throw new ResourceNotFoundError('Un error inesperado guardarTemperatura', error.messages || [error.message]);

        }

    }

    /**
     * Obtener temperatura según el Id
     * @param {string} idTemperatura - Id de temperatura
     * @param {boolean} completarRef - Flag para completar los datos de referencia a otro documento
     * @return {object} Documento temperatura
     */
    static async buscarTemperaturaPorId(idTemperatura, completarRef = false) {

        /**
         * Filtro de búsqueda
         */
        const filtro = {
            _id: idTemperatura,
            activo: true
        };

        try {

            // Obtención de temperatura según filtro
            /**
             * Resultado de busqueda de documento
             */
            let res = {};

            // Verificar si se completan las referencias del documento
            if (completarRef) {

                res = await Temperatura.findOne(filtro).populate('colaborador_id').populate('creado_por_id');

            } else {

                res = await Temperatura.findOne(filtro);

            }

            return res;

        } catch (error) {

            // Lanzar error ante problema en la obtención de temperatura
            throw new ResourceNotFoundError('Un error inesperado buscarTemperaturaPorId', error.messages || [error.message]);

        }

    }

    /**
     * Obtener temperatura según la fecha
     * @param {string} fecha - fecha de temperatura
     * @param {boolean} completarRef - Flag para completar los datos de referencia a otro documento
     * @return {object} Array de documentos temperatura
     */
    static async buscarTemperaturaPorFecha(fecha, completarRef = false) {

        /**
         * Fecha siguiente a la fecha a buscar
         */
        const fechaSiguiente = util.sumarDiasFecha(fecha, 1);

        /**
         * Filtro de búsqueda
         */
        const filtro = {
            fecha_registro: { $gte: fecha, $lt: fechaSiguiente },
            activo: true
        };

        try {

            // Obtención de temperatura según filtro
            /**
             * Resultado de busqueda de documento
             */
            let res = {};

            // Verificar si se completan las referencias del documento
            if (completarRef) {

                res = await Temperatura.find(filtro).populate('colaborador_id').populate('creado_por_id');

            } else {

                res = await Temperatura.find(filtro);

            }

            return res;

        } catch (error) {

            // Lanzar error ante problema en la obtención de temperatura
            throw new ResourceNotFoundError('Un error inesperado buscarTemperaturaPorId', error.messages || [error.message]);

        }

    }

    /**
     * Obtener temperatura según la fecha y colaborador
     * @param {string} fecha - fecha de temperatura
     * @param {boolean} completarRef - Flag para completar los datos de referencia a otro documento
     * @return {object} Array de documentos temperatura
     */
    static async buscarTemperaturaPorColaboradorFecha(idColaborador, fecha, completarRef = false) {

        /**
         * Fecha siguiente a la fecha a buscar
         */
        const fechaSiguiente = util.sumarDiasFecha(fecha, 1);

        /**
         * Filtro de búsqueda
         */
        const filtro = {
            colaborador_id: idColaborador,
            fecha_registro: { $gte: fecha, $lt: fechaSiguiente },
            activo: true
        };

        console.log(filtro);

        try {

            // Obtención de temperatura según filtro
            /**
             * Resultado de busqueda de documento
             */
            let res = {};

            // Verificar si se completan las referencias del documento
            if (completarRef) {

                res = await Temperatura.find(filtro).populate('colaborador_id').populate('creado_por_id');

            } else {

                res = await Temperatura.find(filtro);

            }

            return res;

        } catch (error) {

            // Lanzar error ante problema en la obtención de temperatura
            throw new ResourceNotFoundError('Un error inesperado buscarTemperaturaPorId', error.messages || [error.message]);

        }

    }

}

module.exports.TemperaturaDefaultDb = TemperaturaDefaultDb;