const { ResourceNotFoundError} = require("../error/BusinessError");
const { Alta } = require('../models/alta');
const {Caso}=require('../models/caso');
const alertas=require("../util/alertas");

class AltaDefaultDb{


    static async registrarAlta(Alta){
        try{
            let alta = await Alta.save();
            alertas.alertaAltaEpid(alta);
            return alta;
        }catch(error){
            throw new ResourceNotFoundError('Un error inesperado altaRegistrar', error.messages || [error.message]);  
        }
    }

    static async obtenerAltaPorId(idAlta) {
        try {
            return await Alta.findById(idAlta).lean();
        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }

    static async listarAlta(params) {
        try {
            return await Alta.find().lean().sort({fecha_alta:'desc'});
        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }

    static async listarAltaPorCaso(params){
        try{
            return await Caso.find({$or:[{caso_alta_epid:true,status:true},{caso_finalizado:true,status:true}]}).lean().sort({fecha_alta:'desc'}).select({test:1, caso_tipo:1, caso_finalizado:1, colaborador:1,fecha_inico_caso:1,fecha_alta_epid:1,fecha_alta_medica:1,fecha_inicio_caso:1});
        }catch(error){
            console.log('Error:', error);
            throw new Error();
        }

    }

}

module.exports.AltaDefaultDb=AltaDefaultDb;