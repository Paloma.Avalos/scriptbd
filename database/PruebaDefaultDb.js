const { Prueba } = require('../models/prueba');
const { ResourceNotFoundError, InternalError } = require("../error/BusinessError");
const constante = require('../util/constante');
const util = require('../util/util');

class PruebaDefaultDb {


    static async grabarPrueba(Prueba) {

        try {
            return await Prueba.save();
        } catch (error) {
            throw new ResourceNotFoundError('Un error inesperado actualizarUserSave', error.messages || [error.message]);
        }
    }

    static async obtenerPruebaPorId(idPrueba) {

        try {
            return await Prueba.find({ '_id': idPrueba, 'status': true }).lean();
        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }

    static async obtenerPruebasPaginado(params) {

        console.log(params);

        let tipo_documento = params.tipo_documento;
        let numero_documento = params.nro_documento;
        let nombre_completo = params.nombre_completo;
        let id_colaborador = params.idTrabajador;
        let tipo_carga_prueba = params.tipo_carga_prueba;

        let fecha_programacion = params.fecha_programacion;


        try {

            if (!util.esVacio(tipo_documento) && !util.esVacio(numero_documento)) {
                return await Prueba.aggregate(
                    [{
                        $match: {
                            "$and": [{
                                "colaborador.numero_documento": {
                                    $regex: numero_documento,
                                    $options: 'i'
                                }
                            }, { "fecha_realizacion": { "$eq": null } }]
                        }
                    }
                        ,
                    {
                        $sort: { fecha_programacion: -1 }
                    }
                    ]
                )
            } else if (!util.esVacio(nombre_completo)) {
                return await Prueba.aggregate(
                    [{
                        $match: {
                            "colaborador.nombre_completo": {
                                $regex: nombre_completo,
                                $options: 'i'
                            }
                        }
                    },
                    {
                        $sort: { fecha_realizacion: -1 }
                    }
                    ]
                )
            } else if (!util.esVacio(tipo_carga_prueba)) {
                tipo_carga_prueba = tipo_carga_prueba.toUpperCase();
                if (tipo_carga_prueba == constante.TIPO_CARGA_PRUEBA.REALIZADA) {
                    return await Prueba.find({ "$and": [{ "tipo_carga_prueba": { "$eq": tipo_carga_prueba } }, { "fecha_realizacion": { "$ne": null } }] }).lean().sort({ fecha_realizacion: 'desc' });
                }
                else if (tipo_carga_prueba == constante.TIPO_CARGA_PRUEBA.PENDIENTE) {
                    return await Prueba.find({ "$and": [{ "tipo_carga_prueba": { "$eq": tipo_carga_prueba } }, { "fecha_programacion": { "$ne": null } }] }).lean().sort({ fecha_programacion: 'desc' });
                }
            }
            else if (!util.esVacio(fecha_programacion)) {
                let fecha_prog_limite = util.obtenerFechaUtcFormat(fecha_programacion);
                return await Prueba.find({ $and: [{ "fecha_programacion": { $lte: fecha_prog_limite } }] }).lean().sort({ fecha_programacion: 'desc' });
            } else if (!util.esVacio(tipo_documento)) {
                return await Prueba.aggregate(
                    [{
                        $match: {
                            //"$and": [{
                                "colaborador.tipo_documento": {
                                    $regex: tipo_documento,
                                    $options: 'i'
                                }
                            //}/*, { "fecha_realizacion": { "$eq": null } }*///]
                        }
                    }
                        ,
                    {
                        $sort: { fecha_programacion: -1 }
                    }
                        ,
                    {
                        $group: {
                            tipo_carga_prueba: {$first:'$tipo_carga_prueba'}
                        }
                    }
                    //     ,
                    // {
                    //     $project: {
                    //         tipo_carga_prueba: 1
                    //     }
                    // }
                    ]
                )

                //return await Prueba.find({ "$and": [{ "tipo_carga_prueba": { "$eq": tipo_carga_prueba } },
                // { "fecha_realizacion": { "$ne": null } }] }).lean().sort({ fecha_realizacion: 'desc' });


                // idTrabajador = idTrabajador.charAt(0).toUpperCase()  + idTrabajador.slice(1).toLowerCase();
                // return await Prueba.find({"id_colaborador": idTrabajador}).sort({ fecha_realizacion: -1 });
            } else {
                return await Prueba.find().sort({ fecha_realizacion: 'asc' });
            }
        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }


    static async obtenerPruebas() {

        try {

            return await Prueba.find({ status: true }).sort({ fecha_programacion: 'desc' });

        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }

    static async obtenerPruebasPorIdTrabajadorStatusTrue(idTrabajador) {

        try {
            return await Prueba.find({ 'id_colaborador': idTrabajador, 'status': true }).lean();
        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }

    static async obtenerPruebasPorIdTrabajadorPaginado(idTrabajador, limite, pagina) {

        try {
            const query = { 'id_colaborador': new RegExp(idTrabajador, "i") }
            query.id_colaborador = idTrabajador;
            return await Prueba.paginate(query, {
                page: pagina,
                limit: limite
            });
        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }

    static async actualizarPrueba(idPrueba, prueba) {
        try {
            return await Prueba.findByIdAndUpdate(idPrueba, prueba);
        } catch (error) {
            console.log('Error:', error);
            throw new ResourceNotFoundError('Un error inesperado pruebaActualizar', error.messages || [error.message]);
        }
    }

    static async actualizarPDFPrueba(idPrueba, url_prueba) {
        try {
            return await Prueba.findByIdAndUpdate(idPrueba, { "$set": { url_prueba: url_prueba } });
        } catch (error) {
            console.log('Error:', error);
            throw new ResourceNotFoundError('Un error inesperado pruebaActualizar', error.messages || [error.message]);
        }
    }

    static async obtenerPruebasProgramadas() {
        try {
            let filter = {
                $in: [undefined, null, ""]
            }
            return await Prueba.find({ "resultado": filter, "fecha_realizacion": filter, "id_resultado": filter, "fecha_resultado": filter })
        } catch (error) {
            console.log('Error', error);
            throw new Error();
        }
    }
}

module.exports.PruebaDefaultDb = PruebaDefaultDb;