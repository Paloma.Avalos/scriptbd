const {Caso} = require('../models/caso');
const { ResourceNotFoundError} = require("../error/BusinessError");
const util = require('../util/util');
const alertas=require("../util/alertas");


class CasoDefaultDb{

    static async obtenerCasoExistente(id_colaborador, lectura = true) {
        try {
            if (lectura) {
            return await Caso.findOne({'id_colaborador':id_colaborador,'caso_finalizado':false,'status':true}).lean();
            } else {
                return await Caso.findOne({ 'id_colaborador': id_colaborador, 'caso_finalizado': false, 'status': true });
            }
        }catch(error){
            console.log('Error:',error);
            throw new Error();
        }
    }

    static async grabarCasoManual(Caso){
        try{
            let caso = await Caso.save();
            console.log("GrabarCaso");
            alertas.alertaCaso(Caso);
            return await caso;
        }catch(error){
            throw new ResourceNotFoundError('Un error inesperado casoCrearManual', error.messages || [error.message]);

        }
    }

    static async listarTodosCasos(params){
        console.log(params);
        let tipo = params.tipo;
        let caso_finalizado=(params.caso_finalizado=='true');
        let numero_documento=params.numero_documento;
        let nombre_completo= params.nombre_completo;
        let situacion = params.situacion;
        console.log(situacion);
        let sintomatologia = params.sintomatologia;
        let riesgo_exposicion = params.riesgo_exposicion;
        try{
            
            if(!util.esVacio(numero_documento)){


                return await Caso.aggregate(
                    [{
                            $match: {
                                "colaborador.numero_documento": {
                                 $regex: numero_documento,
                                   $options: 'i'
                                }
                            }
                        },
                        {
                            $sort: { fecha_creacion: -1 }
                        },
                        
                        {
                            $group: {
                                _id:'$id_colaborador',
                                id_Object:{$first:'$_id'},
                                id_colaborador:{$first:'$id_colaborador'},
                                colaborador:{$first:'$colaborador'},
                                caso_gruporiesgo:{$first:'$caso_gruporiesgo'},
                                fecha_creacion:{$first:'$fecha_creacion'},
                                fecha_inicio_caso:{$first:'$fecha_inicio_caso'},
                                fecha_atencion:{$first:'$fecha_atencion'},
                                fecha_alta_epid:{$first:'$fecha_alta_epid'},
                                fecha_alta_medica:{$first:'$fecha_alta_medica'},
                                caso_alta_epid:{$first:'$caso_alta_epid'},
                                caso_tipo:{$first:'$caso_tipo'},
                                caso_situacion:{$first:'$caso_situacion'},
                                caso_atencion:{$first:'$caso_atencion'},
                                caso_finalizado:{$first:'$caso_finalizado'},
                                status:{$first:'$status'},
                                test:{$first:'$test'}
                                
                                }
                        },
                        {
                            $project: {
                            _id: "$id_Object",
                            id_colaborador: "$_id",
                            colaborador:1,
                            caso_gruporiesgo:1,
                            fecha_creacion:1,
                            fecha_inicio_caso:1,
                            fecha_atencion:1,
                            fecha_alta_epid:1,
                            fecha_alta_medica:1,
                            caso_alta_epid:1,
                            caso_tipo:1,
                            caso_situacion:1,
                            caso_atencion:1,
                            caso_finalizado:1,
                            status:1,
                            test:1
                         }
                      }
                        
                    ]


                )


            }else if(!util.esVacio(tipo)){
                return await Caso.find({status:true,caso_finalizado:false,caso_tipo:tipo.toUpperCase()}).sort({fecha_atencion:1});


            }else if(caso_finalizado){
                return await Caso.aggregate(
                    [{
                            $match: { caso_finalizado: true }
                        },
                        {
                            $sort: { fecha_creacion: -1 }
                        },
                        
                        {
                            $group:
                            
                            {
                                _id:'$id_colaborador',
                                id_Object:{$first:'$_id'},
                                id_colaborador:{$first:'$id_colaborador'},
                                colaborador:{$first:'$colaborador'},
                                caso_gruporiesgo:{$first:'$caso_gruporiesgo'},
                                fecha_creacion:{$first:'$fecha_creacion'},
                                fecha_inicio_caso:{$first:'$fecha_inicio_caso'},
                                fecha_atencion:{$first:'$fecha_atencion'},
                                fecha_alta_epid:{$first:'$fecha_alta_epid'},
                                fecha_alta_medica:{$first:'$fecha_alta_medica'},
                                caso_alta_epid:{$first:'$caso_alta_epid'},
                                caso_tipo:{$first:'$caso_tipo'},
                                caso_situacion:{$first:'$caso_situacion'},
                                caso_atencion:{$first:'$caso_atencion'},
                                caso_finalizado:{$first:'$caso_finalizado'},
                                status:{$first:'$status'},
                                test:{$first:'$test'}
                                
                                }
                         },
                        {
                            $project: {
                            _id: "$id_Object",
                            id_colaborador: "$_id",
                            colaborador:1,
                            caso_gruporiesgo:1,
                            fecha_creacion:1,
                            fecha_inicio_caso:1,
                            fecha_atencion:1,
                            fecha_alta_epid:1,
                            fecha_alta_medica:1,
                            caso_alta_epid:1,
                            caso_tipo:1,
                            caso_situacion:1,
                            caso_atencion:1,
                            caso_finalizado:1,
                            status:1,
                                test: 1
                            }
                         }
                         
                        
                    ]


                )

            }else if(nombre_completo){
                 return await Caso.aggregate(
                    [{
                            $match: {
                                "colaborador.nombre_completo": {
                                 $regex: nombre_completo,
                                   $options: 'i'
                                }
                            }
                        },
                        {
                            $sort: { fecha_creacion: -1 }
                        },
                        
                        {
                            $group:
                            
                            {
                                _id:'$id_colaborador',
                                id_Object:{$first:'$_id'},
                                id_colaborador:{$first:'$id_colaborador'},
                                colaborador:{$first:'$colaborador'},
                                caso_gruporiesgo:{$first:'$caso_gruporiesgo'},
                                fecha_creacion:{$first:'$fecha_creacion'},
                                fecha_inicio_caso:{$first:'$fecha_inicio_caso'},
                                fecha_atencion:{$first:'$fecha_atencion'},
                                fecha_alta_epid:{$first:'$fecha_alta_epid'},
                                fecha_alta_medica:{$first:'$fecha_alta_medica'},
                                caso_alta_epid:{$first:'$caso_alta_epid'},
                                caso_tipo:{$first:'$caso_tipo'},
                                caso_situacion:{$first:'$caso_situacion'},
                                caso_atencion:{$first:'$caso_atencion'},
                                caso_finalizado:{$first:'$caso_finalizado'},
                                status:{$first:'$status'},
                                test:{$first:'$test'}
                                
                                }
                         },
                        {
                            $project: {
                            _id: "$id_Object",
                            id_colaborador: "$_id",
                            colaborador:1,
                            caso_gruporiesgo:1,
                            fecha_creacion:1,
                            fecha_inicio_caso:1,
                            fecha_atencion:1,
                            fecha_alta_epid:1,
                            fecha_alta_medica:1,
                            caso_alta_epid:1,
                            caso_tipo:1,
                            caso_situacion:1,
                            caso_atencion:1,
                            caso_finalizado:1,
                            status:1,
                                test: 1
                            }
                         }
                         
                        
                    ]


                )

            } else if (situacion) {
                return await Caso.find({ caso_situacion: situacion, caso_finalizado: false, status: true }).sort({ fecha_atencion: 1 });
            } else if (sintomatologia) {

                sintomatologia = sintomatologia.charAt(0).toUpperCase()  + sintomatologia.slice(1).toLowerCase();
                return await Caso.find({"test.resultado_sintomatologico.valor": sintomatologia, caso_finalizado: false, status: true }).sort({ fecha_atencion: 1 });

            } else if (riesgo_exposicion) {
                return await Caso.find({ "colaborador.riesgo_exposicion": riesgo_exposicion, caso_finalizado: false, status: true }).sort({ fecha_atencion: 1 });

            }else{
                return await Caso.find({status:true,caso_finalizado:false}).sort({fecha_atencion:1});

            }
        }catch(error){
            console.log('Error:', error);
            throw new Error();
        }


    }

    
    static async actualizarSituacionCaso(idCaso,situacion){
        try{
            return await Caso.findByIdAndUpdate(idCaso,{"$set":{caso_situacion:situacion}});
        }catch(error){
            console.log('Error:', error);
            throw new Error(); 
        }

    }

    static async obtenerCasoPorId(idCaso){
        try{
            return await Caso.findById(idCaso);
        }catch(error){
            console.log('Error',error);
            throw new Error();
        }
    }

    static async obtenerCasosCerrados(id_colaborador){
        try{
            return await Caso.find({id_colaborador:id_colaborador,caso_finalizado:true}).sort({fecha_creacion:-1});
        }catch(error){
            console.log('Error',error);
            throw new Error();
        }
    }
    static async actualizarUltimaAtencion(idCaso, caso_atencion){
        try{

            return await Caso.findByIdAndUpdate(idCaso,{"$set":{caso_atencion:caso_atencion,fecha_atencion:Date.now()}})

        }catch(error){
            console.log('Error',error);
            throw new Error();
        }

    }

    static async registrarAltaEpid(id_caso,fecha_alta_epid){
        try{
               
            return await Caso.findByIdAndUpdate(id_caso,{"$set":{fecha_alta_epid:fecha_alta_epid,caso_alta_epid:true}})
        }catch(error){

            console.log('Error',error);
            throw new Error();
        }

    }

    static async cerrarCaso(id_caso,fecha_alta_medica){
        try{
            let caso = await Caso.findById(id_caso);
            alertas.alertaCierreCaso(caso);
            return await Caso.findByIdAndUpdate(id_caso,{"$set":{fecha_alta_medica:fecha_alta_medica,caso_finalizado:true}})
        }catch(error){
            console.log('Error',error);
            throw new Error();
        }

    }  
    static async actualizarTipoCaso(id_caso, motivo) {
        try {
            return await Caso.findByIdAndUpdate(id_caso, { '$set': { caso_tipo: motivo } }, { new: true })
        } catch (error) {
            console.log('Error', error);
            throw new Error();
        }
    }
}

module.exports.CasoDefaultDb = CasoDefaultDb;