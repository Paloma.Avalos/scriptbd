
const { ResourceNotFoundError} = require("../error/BusinessError");


class SituacionDefaultDb{

    static async actualizarSituacion(Situacion){
        try{
            return await Situacion.save();
        }catch(error){
            throw new ResourceNotFoundError('Un error inesperado situacionActualizar', error.messages || [error.message]);  
        }

    }




}

module.exports.SituacionDefaultDb=SituacionDefaultDb;