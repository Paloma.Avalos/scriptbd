const {Nota} = require('../models/nota.js');
const {ResourceNotFoundError} = require("../error/BusinessError");

class NotaDb {

    /**
     * Obtener Nota según el Id
     * @param {string} idNota - Id de nota
     */
    static async buscarNotaPorId(idNota) {

        /**
         * Filtro de búsqueda
         */
        const filtro = {
            _id: idNota,
            activo: true
        };

        try {

            // Obtención de nota según filtro
            /**
             * Resultado de busqueda de documento
             */
            let res = {};

            res = await Nota.findOne(filtro);

            return res;

        } catch (error) {

            // Lanzar error ante problema en la obtención de nota
            throw new ResourceNotFoundError('Un error inesperado buscarNotaPorId', error.messages || [error.message]);

        }

    }

}

module.exports.NotaDb = NotaDb;
