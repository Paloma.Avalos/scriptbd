const { Configuracion } = require('../models/configuracion');
const { ResourceNotFoundError } = require("../error/BusinessError");

class ConfiguracionDefaultDb {

    static async buscarConfiguracionActiva() {

        const filtro = {
            activo: true
        };

        try {

            // Obtención de configuración según filtro
            /**
             * Resultado de busqueda de documento
             */
            let res = {};

            res = await Configuracion.findOne(filtro);

            return res;

        } catch (error) {

            // Lanzar error ante problema en la obtención de configuracion
            throw new ResourceNotFoundError('Un error inesperado buscarConfiguracionActiva', error.messages || [error.message]);

        }

    }

}

module.exports.ConfiguracionDefaultDb = ConfiguracionDefaultDb;