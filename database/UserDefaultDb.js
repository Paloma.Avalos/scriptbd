const _ = require('lodash');
const { User } = require('../models/usuario.js');
const { ResourceNotFoundError } = require("../error/BusinessError");

class UserDefaultDb {

    static async buscarUsuarioPorId(idTrabajador) {

        try {
            return await User.findById(idTrabajador);
        } catch (error) {
            throw new ResourceNotFoundError('No se encontro buscarUsuarioPorId', error.messages || [error.message]);
        }
    }

    static async actualizarUserSave(user) {

        try {
            return await user.save();
        } catch (error) {
            throw new ResourceNotFoundError('Un error inesperado actualizarUserSave', error.messages || [error.message]);
        }
    }

    static async buscarUsuarioPorCorreo(correo) {

        try {
            return await User.findOne({ 'correo_electronico': correo });
        } catch (error) {
            throw new ResourceNotFoundError('No se encontro buscarUsuarioPorCorreo', error.messages || [error.message]);
        }
    }

    static async buscarUsuariosPorCorreo(correos) {

        try {
            return await User.find({ 'correo_electronico': { $in: correos } }).select({ _id: 1, correo_electronico: 1, numero_documento: 1 }).lean();
        } catch (error) {
            throw new ResourceNotFoundError('No se encontro buscarUsuarioPorCorreo', error.messages || [error.message]);
        }
    }

    static async buscarUsuarioPorNroDocumento(numero_documento) {

        try {
            return await User.findOne({ 'numero_documento': numero_documento });
        } catch (error) {
            throw new ResourceNotFoundError('No se encontro el buscarUsuarioPorNroDocumento', error.messages || [error.message]);
        }
    }

    static async grabarUser(user) {

        try {
            return await user.save();
        } catch (error) {
            throw new ResourceNotFoundError('Un error inesperado grabarUser', error.messages || [error.message]);
        }
    }

    static async actualizarResultadoMedico(id_colaborador, resultado_medico) {

        try {
            let item = {
                "condiciones": resultado_medico
            }
            let user = await User.findById(id_colaborador);
            user.resultado_medico = _.concat(user.resultado_medico, item)
            return await User.findByIdAndUpdate(id_colaborador, { "$set": { resultado_medico: user.resultado_medico } })

        } catch (error) {
            console.log(error);
            throw new ResourceNotFoundError('Un error inesperado actualizarResultadoMedico', error.messages || [error.message]);

        }

    }

    /**
     * Obtener usuario según un tipo de documento y un número de documento dado
     * @param {string} tipoDocumento - Tipo de documento de colaborador
     * @param {string} numeroDocumento - Número de documento de colaborador
     * @param {boolean} activo - Flag de búsqueda de documentos activos
     * @return {object} Documento usuario
     */
    static async buscarUsuarioPorTipoNroDocumento(tipoDocumento, numeroDocumento, activo) {

        /**
         * Filtro de búsqueda
         */
        const filtro = {
            tipo_documento: tipoDocumento,
            numero_documento: numeroDocumento,
            activo: activo
        };

        try {

            // Obtención del colaborador según filtro
            return await User.findOne(filtro);

        } catch (error) {

            // Lanzar error ante problema en la obtención de usuario
            throw new ResourceNotFoundError('Un error inesperado buscarUsuarioPorTipoNroDocumento', error.messages || [error.message]);

        }
        
    }


    static async grabarBloques(bulkUpdateOps) {
        try {
            return await User.bulkWrite(bulkUpdateOps);
        } catch (error) {
            console.log("ERROR:", JSON.stringify(error));
            return error;
        }
    }
}

module.exports.UserDefaultDb = UserDefaultDb;