const constante = require('../util/constante');
const mongoose = require('mongoose');
mongoose
    .connect(constante.MY_CONNECTION, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(result => {
        //console.log('Se ha realizado la conexion...', result);
        console.log('Se ha realizado la conexion');
        mongoose.set('debug', constante.MONGOOSE_DEBUG);
    })
    .catch(err => console.log(err));

module.exports.Connection = mongoose;