const { Cuarentena } = require('../models/cuarentena');
const { ResourceNotFoundError, InternalError } = require("../error/BusinessError");
const { isNull, isEqual } = require('lodash');

class CuarentenaDefaultDb {


    static async grabarCuarentena(Cuarentena) {

        try {
            return await Cuarentena.save();
        } catch (error) {
            throw new ResourceNotFoundError('Un error inesperado actualizarUserSave', error.messages || [error.message]);
        }
    }

    static async obteneridCuarentenaPorId(idCuarentena) {

        try {
            return await Cuarentena.findById(idCuarentena).lean();
        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }

    static async obteneridCuarentenaPorIdTrabajador(idTrabajador) {

        try {
            return await Cuarentena.find({ 'id_colaborador': idTrabajador }).lean();
        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }
    }
	
    static async obtenerCuarentenaPorColaborarCaso(request) {
        try {
            const id_colaborador = request.query.id_colaborador;
            const id_caso = request.query.id_caso;
          
            if (request.query.id_colaborador!='null') {
                return await Cuarentena.find({"id_colaborador" : id_colaborador}).lean();
            }
            else if(request.query.id_caso!='null') {
                return await Cuarentena.find({"id_caso" : id_caso}).lean();
            }            
        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }    
    }

    static async actualizarCuarentena(idCuarentena,cuarentena ){
        try{
            return await Cuarentena.findByIdAndUpdate(idCuarentena,cuarentena);
        }catch(error){
            console.log('Error:', error);
            throw new ResourceNotFoundError('Un error inesperado CuarentenaActualizar', error.messages || [error.message]);  
        }
    }

}

module.exports.CuarentenaDefaultDb = CuarentenaDefaultDb;