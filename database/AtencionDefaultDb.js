const { ResourceNotFoundError} = require("../error/BusinessError");
const { Atencion } = require('../models/atencion');

class AtencionDefaultDb{

    static async registrarAtencion(Atencion){
        try{
            return await Atencion.save();
        }catch(error){
            throw new ResourceNotFoundError('Un error inesperado atencionRegistrar', error.messages || [error.message]);  
        }

    }

    static async obtenerAtencionPorCaso(atencion) {
        try {
            
            const id_caso = atencion.query.id_caso;
          
            if(atencion.query.id_caso!='null') {
                return await Atencion.find({"id_caso" : id_caso}).lean();
            }            
        } catch (error) {
            console.log('Error:', error);
            throw new Error();
        }    
    }

}






module.exports.AtencionDefaultDb=AtencionDefaultDb;