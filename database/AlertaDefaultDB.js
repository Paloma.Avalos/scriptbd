const { Alerta } = require('../models/alerta');
const { ResourceNotFoundError } = require("../error/BusinessError");
const util = require('../util/util');

class AlertaDefaultDb{

    static async registrarAlerta(Alerta){

        try {
            return await Alerta.save();
        } catch (error) {
            throw new ResourceNotFoundError('Un error inesperado registrarAlerta', error.messages || [error.message]);
        }

    }

    static async obtenerAlerta(){

        try {
            return await Alerta.find({status:true});
        } catch (error) {
            throw new ResourceNotFoundError('Un error inesperado obtenerAlerta', error.messages || [error.message]);
        }
    }

    static async actualizarAlertaById(idAlerta,alerta ){
        try{
            return await Alerta.findByIdAndUpdate(idAlerta,alerta);
        }catch(error){
            console.log('Error:', error);
            throw new ResourceNotFoundError('Un error inesperado ActualizarAlerta', error.messages || [error.message]);  
        }
    }

    static async obtenerAlertaById(idAlerta){

        try {
            return await Alerta.findOne({'_id':idAlerta}).lean();
        } catch (error) {
            throw new ResourceNotFoundError('Un error inesperado obtenerAlerta', error.messages || [error.message]);
        }
    }
}

module.exports.AlertaDefaultDb=AlertaDefaultDb;