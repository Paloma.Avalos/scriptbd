const { Test } = require('../models/test');
const { ResourceNotFoundError } = require("../error/BusinessError");

class TestDefaultDb {

    /**
     * Obtener test según el Id
     * @param {string} idTest - Id de test
     * @return {object} Documento test
     */
    static async buscarTestPorId(idTest) {

        try {

            // Obtención de temperatura según filtro
            return await Test.findById(idTest);

        } catch (error) {

            // Lanzar error ante problema en la obtención de temperatura
            throw new ResourceNotFoundError('Un error inesperado buscarTestPorId', error.messages || [error.message]);

        }

    }

    static async buscarUltimoTestPorIdColaborador(idColaborador) {

        try {

            return await Test.findOne({ 'id_colaborador': idColaborador }).sort({ fecha_creacion: 'desc' });

        } catch (error) {

            // Lanzar error ante problema en la obtención de temperatura
            throw new ResourceNotFoundError('Un error inesperado buscarUltimoTestPorIdColaborador', error.messages || [error.message]);

        }

    }

    static async listTestPorIdColaborador(idColaborador) {

        try {

            return await Test.find({ 'id_colaborador': idColaborador }).sort({ fecha_creacion: 'desc' });

        } catch (error) {

            throw new ResourceNotFoundError('Un error inesperado buscarTestPorIdColaborador', error.messages || [error.message]);

        }

    }


    static async actualizarTestSave(test) {

        try {
            test.markModified('resultado_cuestionario');
            return await test.save();
        } catch (error) {
            throw new ResourceNotFoundError('Un error inesperado actualizarUserSave', error.messages || [error.message]);
        }
    }

}

module.exports.TestDefaultDb = TestDefaultDb;