const mongoose = require('mongoose');
const { Colaborador } = require('./models/subDocument/colaborador');
const _ = require('lodash');
const util = require('./util/util');

async function main() {

    console.log("Iniciando proceso de validación de la situación del colaborador");

    //CONEXION A BASE DATOS
    await mongoose.connect("mongodb+srv://healthcaredev:Admin123@clustercompanya-9vbux.azure.mongodb.net/healthcare-cte?authSource=admin&replicaSet=ClusterCompanyA-shard-0&readPreference=primary&ssl=true", {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    //Obtener todos los colaboradores
    const listColaboradorTodos = await Colaborador.find({ activo: true }).lean();
    console.log("CANTIDAD DE COLABORADORES OBTENIDOS: " + listColaboradorTodos.length);


    //Obtener todos los colaboradores que tengan resultado_medico
    for (let i = 0; i < listColaboradorTodos.length; i++) {
        console.log("Se recorre el registro Nro : " + i + "  ----  " + listColaboradorTodos[i].numero_documento);

        //validamos que cuente con resultado medico lleno
        if (!util.esVacio(listColaboradorTodos[i].resultado_medico)) {
            //contamos cuantos resultados tiene
            let countColaboradorRM = listColaboradorTodos[i].resultado_medico.length;

            // Obtenemos todos los valores del resultado medico
            let colaboradorSubSchema = await obtenerColaboradorSubSchema(listColaboradorTodos[i]);
            let DNI = listColaboradorTodos[i].numero_documento;

            for (let j = 0; j < countColaboradorRM; j++) {

                let condiciones = listColaboradorTodos[i].resultado_medico[j].condiciones;

                if (condiciones == "Alta epidemiológica" || condiciones == "ALTA EPIDEMIOLÓGICA") {

                    listColaboradorTodos[i].resultado_medico[j].condiciones = "Alta";
                    await Colaborador.updateOne({ numero_documento: DNI }, { "$set": { resultado_medico: colaboradorSubSchema.resultado_medico } });

                }
            }
            console.log("Se actualizó el colaborador con DNI  : " + DNI);

        }
    }

}

async function obtenerColaboradorSubSchema(usuario) {
    let user = _.pick(usuario, ['resultado_medico']);
    user.resultado_medico = usuario.resultado_medico;
    return user;
}

main()
    .then(v => console.log("Se termino la carga de casos"))
    .catch(err => console.error(err));