const mongoose = require('mongoose');
const CSVToJSON = require('csvtojson');
const { Caso } = require('./models/caso');
const _ = require('lodash');
const constante = require('./util/constante')


async function main() {
    console.log("Iniciando proceso de validación de caso");

    await mongoose.connect("mongodb+srv://healthcaredev:Admin123@clustercompanya-9vbux.azure.mongodb.net/healthcare-cte?authSource=admin&replicaSet=ClusterCompanyA-shard-0&readPreference=primary&ssl=true", {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    const testCaso = await CSVToJSON().fromFile('./OutputCsv/tipoCaso.csv');
    
    const listCasoTodos = await Caso.find({ caso_tipo: "POSITIVO COVID" });
    console.log("listCasoTodos" + listCasoTodos.length);
    
    console.log(testCaso.length);
    
    await Caso.updateMany( {caso_tipo: "POSITIVO COVID"}, {"$set": { caso_tipo: "CONFIRMADO"} } );
    
}


async function obtenerCaso(caso) {
    console.log("obtenerCaso : ", caso);
    caso.caso_tipo = constante.CONFIRMADO;//"CONFIRMADO";
    return caso;
}

main()
    .then(v => console.log("Se termino la carga de casos"))
    .catch(err => console.error(err));