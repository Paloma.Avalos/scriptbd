const mongoose = require('mongoose');
const path = require('path');
const fs = require('fs');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const CSVToJSON = require('csvtojson');
const { Caso } = require('./models/caso');
const { Alta } = require('./models/alta');
const _ = require('lodash');
const constante = require('./util/constante')
const util = require('./util/util');

const caso = {
    caso_finalizado: String,
    caso_tipo: String
};

async function main() {

    console.log("Iniciando proceso de validación de caso");

    //CONEXION A BASE DATOS
    await mongoose.connect("mongodb+srv://healthcaredev:Admin123@clustercompanya-9vbux.azure.mongodb.net/healthcare-cte?authSource=admin&replicaSet=ClusterCompanyA-shard-0&readPreference=primary&ssl=true", {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    const CasoColaborador = await CSVToJSON().fromFile('./OutputCsv/colaboradorDNI.csv');

    console.log("Cantidad de CasosColaborador a actualizar: " + CasoColaborador.length);

    for (let i = 0; i < CasoColaborador.length; i++) {

        console.log("CasoColaborador - Posición : " + [i] + " - " + CasoColaborador[i]["numero_documento"]);
            
        //Validamos si existen altas con el Nro de documento del colaborador
        let alta = await Alta.find({ "colaborador.numero_documento": CasoColaborador[i]["numero_documento"] }).lean().sort({ "fecha_creacion": 'desc' });
       
        //validamos que existen alta(s)
        if (!util.esVacio(alta)) {
            
            //Obtenemos el IdCaso
            let AltaByCaso = alta[0].id_caso;  
            let NumDoc = alta[0].colaborador.numero_documento ;
            let idColaborador = alta[0].id_colaborador;
            console.log("AltaByCaso  - IdCaso : " + AltaByCaso);
            console.log("AltaByCaso  - NumDoc : " + NumDoc);
            console.log("AltaByCaso  - idColaborador : " + idColaborador);
            //Buscamos el caso por el IdCaso obtenido
            let getCaso = await Caso.find({"_id": alta[0].id_caso , 
                                           "colaborador.numero_documento": alta[0].colaborador.numero_documento ,
                                           "id_colaborador" : alta[0].id_colaborador}).lean().sort({ "fecha_creacion": 'desc' });
                       
            if (!util.esVacio(getCaso)) {
                console.log("Entró ");
                //Llenamos valores del caso
                let caso_finalizado = getCaso[0].caso_finalizado;
                let caso_tipo = getCaso[0].caso_tipo;
                console.log("Valor de caso_situacion: " + caso_finalizado);
                console.log("Valor de caso_tipo: " + caso_tipo);

                //asignamos los valores al Object Caso
                caso.caso_tipo = caso_tipo;
                caso.caso_finalizado = caso_finalizado;


                let altaid= alta[0]._id;
                console.log("Valor de altaid: " + altaid);

                //asignamos el object caso dentro de la alta
                let AltaUpdate = await Alta.findByIdAndUpdate(alta[`0`]._id,{"$set":{caso:caso}});
               
            }
        }
    }
}


main()
    .then(v => console.log("Se termino la carga de casos"))
    .catch(err => console.error(err));