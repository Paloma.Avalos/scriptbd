const { AlertaDefaultDb } = require('../database/AlertaDefaultDb');
const constante = require('../util/constante');
const api = require('../util/apiRest');
const alertaCaso = async(caso)=>{

    let item={
        "event":"Apertura de caso",
        "date":caso.fecha_creacion,
        "document":`${caso.colaborador.tipo_documento} - ${caso.colaborador.numero_documento}`,
        "name":`${caso.colaborador.nombres} ${caso.colaborador.apellido_paterno} ${caso.colaborador.apellido_materno}`,
        "description":"CASO SOSPECHOSO",
        "dashboard_url":constante.URI_DASHBOARD_URL
    };
    let users=[];
    let alertas = await AlertaDefaultDb.obtenerAlertaByTipo(2);

    if(alertas.length==0){return true}

    for(let i=0;i<alertas.length;i++){
        users.push({
            "email":alertas[i].email,
            ...item
        })
    }
 
    await api.postSendAlert(users);


}

const alertaAltaEpid=async(alta)=>{

    let item ={
        "event":"Alta epidemiológica",
        "date":alta.fecha_alta,
        "document":`${alta.colaborador.tipo_documento} - ${alta.colaborador.numero_documento}`,
        "name":`${alta.colaborador.nombres} ${alta.colaborador.apellido_paterno} ${alta.colaborador.apellido_materno}`,
        "description":"CASO SOSPECHOSO",
        "dashboard_url":constante.URI_DASHBOARD_URL
    }

    let users=[];
    let alertas = await AlertaDefaultDb.obtenerAlertaByTipo(1);
    if(alertas.length==0){return true}

    for(let i=0;i<alertas.length;i++){
        users.push({
            "email":alertas[i].email,
            ...item
        })
    }
 
    await api.postSendAlert(users);

}

const alertaCierreCaso=async(caso)=>{

    let item ={
        "event":"CIERRE DE CASO",
        "date":caso.fecha_alta_medica,
        "document":`${caso.colaborador.tipo_documento} - ${caso.colaborador.numero_documento}`,
        "name":`${caso.colaborador.nombres} ${caso.colaborador.apellido_paterno} ${caso.colaborador.apellido_materno}`,
        "description":"CASO SOSPECHOSO",
        "dashboard_url":constante.URI_DASHBOARD_URL
    }

    let users=[];
    let alertas = await AlertaDefaultDb.obtenerAlertaByTipo(3);
    if(alertas.length==0){return true}

    for(let i=0;i<alertas.length;i++){
        users.push({
            "email":alertas[i].email,
            ...item
        })
    }
 
    await api.postSendAlert(users);

}


module.exports={
    alertaCaso,
    alertaAltaEpid,
    alertaCierreCaso
}