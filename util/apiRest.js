"use strict";

const axios = require('axios');
const constante = require("../util/constante");

function getApiArea() {
    try {
        console.log('INVOCANDO getApiArea');
        const data = axios.get(constante.URI_AREA);

        return data;
    } catch (error) {
        console.log('ERROR getApiArea:', error);
        throw error;
    }
}

function getApiRiesgo() {
    try {
        console.log('INVOCANDO getApiRiesgo');
        const data = axios.get(constante.URI_RIESGO);

        return data;
    } catch (error) {
        console.log('ERROR getApiRiesgo:', error);
        throw error;
    }
}

function getApiSede() {
    try {
        console.log('INVOCANDO getApiSede');
        const data = axios.get(constante.URI_SEDE);

        return data;
    } catch (error) {
        console.log('ERROR getApiSede:', error);
        throw error;
    }
}


function getApiTipoUsuario() {
    try {
        console.log('INVOCANDO getApiTipoUsuario');
        const data = axios.get(constante.URI_TIPO_USUARIO);

        return data;
    } catch (error) {
        console.log('ERROR getApiTipoUsuario:', error);
        throw error;
    }
}

const getApiTipoUsuarioById = async(id) => {
    try {

        const { data } = await axios.get(constante.URI_TIPO_USUARIO, {
            params: {
                id: id
            }
        });

        return data;
    } catch (error) {
        console.log('ERROR getApiTipoUsuarioById:', error);
        throw error;
    }
};

///PALOMA
const getApiTipoNotificacionById = async(id) => {
    try {

        const { data } = await axios.get(constante.URI_TIPO_NOTIFICACION, {
            params: {
                id: id
            }
        });

        return data;
    } catch (error) {
        console.log('ERROR getApiTipoNotificacionById:', error);
        throw error;
    }
};


function getApiTipoDocumento() {
    try {
        console.log('INVOCANDO getApiTipoDocumento');
        const data = axios.get(constante.URI_TIPO_DOCUMENTO);

        return data;
    } catch (error) {
        console.log('ERROR getApiTipoDocumento:', error);
        throw error;
    }
}

function getApiCentroCosto() {
    try {
        console.log('INVOCANDO getApiCentroCosto');
        const data = axios.get(constante.URI_CENTRO_COSTO);

        return data;
    } catch (error) {
        console.log('ERROR getApiCentroCosto:', error);
        throw error;
    }
}


function getApiUnidad() {
    try {
        console.log('INVOCANDO getApiUnidad');
        const data = axios.get(constante.URI_UNIDAD);

        return data;
    } catch (error) {
        console.log('ERROR getApiUnidad:', error);
        throw error;
    }
}

function getApiPais() {
    try {
        console.log('INVOCANDO getApiPais');
        const data = axios.get(constante.URI_PAIS);

        return data;
    } catch (error) {
        console.log('ERROR getApiPais:', error);
        throw error;
    }
}

const getTiposPrueba = async() => {
    try {
        console.log('INVOCANDO getTiposPrueba');
        const { data } = await axios.get(constante.URI_TIPO_PRUEBA);

        return data;
    } catch (error) {
        console.log('ERROR getTiposPrueba:', error);
        throw error;
    }
}

const postEnviarCorreo = async(arrayCorreos) => {
    try {
        console.log('arrayCorreos:', arrayCorreos);
        const { data } = await axios.post(constante.URI_ENVIO_CORREO, {
            users: arrayCorreos
        });

        return data;
    } catch (error) {
        console.log('ERROR postEnviarCorreo:', error);
    }
};

const postEnviarCorreoNumDoc = async(arrayCorreos) => {
    try {
        console.log('arrayCorreos:', arrayCorreos);
        const { data } = await axios.post(constante.URI_ENVIO_CORREO_NUMDOC, {
            users: arrayCorreos
        });

        return data;
    } catch (error) {
        console.log('ERROR postEnviarCorreoNumDoc:', error);
    }
};

const postObtenerResultadoTriajeDiario = async(uriServicio, request) => {
    try {
        console.log('request:', request);
        const { data } = await axios.post(uriServicio, {
            id_cuestionario: request.id_cuestionario,
            id_colaborador: request.id_colaborador,
            secciones: request.secciones
        });

        return data;
    } catch (error) {
        console.log('ERROR postObtenerResultadoTriajeDiario:', error);
    }
};

const postCrearCaso = async(uriServicio, request) => {

    const config = {
        headers: {
            'Content-Type': 'application/json',
            'x-auth-token': request.token
        }
    };

    try {
        const { data } = await axios.post(uriServicio, {
            id_colaborador: request.id_colaborador,
            fecha_inicio_caso: request.fecha_inicio_caso,
            motivo: request.motivo,
            test: request.test

        }, config);

        return data;
    } catch (error) {
        if (error.response) {
            console.log('error.response.data:', error.response.data);
            return error.response.data;
        } else if (error.request) {
            console.log('error.request:', error.request);
        } else {
            console.error('Error:', error.message);
        }
    }
};

module.exports = {
    getApiArea,
    getApiRiesgo,
    getApiSede,
    getApiTipoUsuario,
    getApiTipoDocumento,
    getApiUnidad,
    getApiPais,
    getApiTipoUsuarioById,
    getApiCentroCosto,
    postEnviarCorreo,
    postEnviarCorreoNumDoc,
    getApiTipoNotificacionById,
    getTiposPrueba,
    postObtenerResultadoTriajeDiario,
    postCrearCaso
}