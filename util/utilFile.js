const multipart = require("parse-multipart");
const csvparser = require('csv-parser');
const stream = require('stream');
const xlsx = require("xlsx");
const constante = require("../util/constante");
const { Prueba } = require("../models/prueba");
const { Cuarentena } = require("../models/cuarentena");
const { Hospitalizacion } = require("../models/hospitalizacion");
const _ = require('underscore');
//const constante = require('../util/constante');
var path = require('path');
const validation = require('../PruebaImportar/validation/prueba.validation');
const util = require("../util/util");
const { SchemaBodyPrueba } = require("../PruebaImportar/validation/prueba.schema");
const account = constante.ACCOUNT_ASSETS;
const accountKey = constante.ACCOUNT_KEY_ASSETS;

// const containerNamePruebas = constante.CONTAINER_PRUEBAS;
// const containerNameFirmas = constante.CONTAINER_FIRMAS;
// const containerNameDocumentos = constante.CONTAINER_DOCUMENTOS;
//const sharedKeyCredential = new StorageSharedKeyCredential(account, accountKey);
// const blobServiceClient = new BlobServiceClient(
//     `https://${account}.blob.core.windows.net`,
//     sharedKeyCredential
// ); 

// const { BlobServiceClient, StorageSharedKeyCredential } = require("@azure/storage-blob");
// const getStream = require('into-stream');


const { ResourceNotFoundError, InternalError } = require("../error/BusinessError");
const { SchemaBodyHospitalizacion } = require("../PruebaImportar/validation/hospitalizacion.schema");
const { SchemaBodyCuarentena } = require("../PruebaImportar/validation/cuarentena.schema");

async function importarUsuarios(req) {
    let buffer = obtnerBuffer(req, '.csv');
    let data = await leerCsv(buffer);
    return data;
}

// IMPORTAR DATA
async function importarExcel(req, hojas, schemas, validation, validarAll = true) {
    let buffer = obtnerBuffer(req, '.xlsx');
    let workbook = obtenerLibro(buffer);
    let resultados = null;

    if (hojas.length == schemas.length && hojas.length == validation.length) {
        resultados = await obtenerResultado(workbook, hojas, schemas, validation, validarAll);
        return resultados;
    } else {
        let mensaje = "El input ingresado no tiene el mismo size, por favor verificar";
        console.log(mensaje);
        throw new InternalError(mensaje);
    }
}

function obtnerBuffer(req, extPermitido) {
    let bodyBuffer = Buffer.from(req.body);
    let boundary = multipart.getBoundary(req.headers['content-type']);
    let parts = multipart.Parse(bodyBuffer, boundary);
    let buffer = parts[0].data;

    let extension = path.extname(parts[0].filename);
    console.log('extension:', extension);
    console.log('filename:', parts[0].filename);
    console.log('type:', parts[0].type);
    console.log('data length:', parts[0].data.length);

    if (extension.toLowerCase() !== extPermitido) {
        throw new InternalError('El tipo de archivo debe ser ' + extPermitido);
    }

    if (parts[0].data.length > 524288) {
        throw new InternalError('El archivo excede el tamaÃ±o permitido ' + (524288 / 1024) + "kb.");
    }
                                 
    return buffer;
        }

async function leerCsv(csvBuffer) {
    try {
        const data = await readBuffer(csvBuffer);
        return data;
    } catch (err) {
        console.log(err);
    }

}

async function readBuffer(buffer) {
    let results = [];
    console.time('Measuring time');
    let bufferstream = new stream.PassThrough();
    bufferstream.end(buffer);
    bufferstream
        .pipe(csvparser())
        .on('data', (data) => {
            results.push(data);
        })
        .on('end', () => {
            //console.log(results);
        });
    console.timeEnd('Measuring time');
    return results;
    }

// IMPORTAR PRUEBAS
async function importarPruebas(req, tipoPrueba) {
    let buffer = obtnerBuffer(req, '.xlsx');
    let workbook = obtenerLibro(buffer);
    let resultados = await obtenerResultados(workbook, tipoPrueba);

    return resultados;
}

function obtenerLibro(buffer) {
    let workbook = xlsx.read(buffer);
    return workbook;
}

async function obtenerResultado(workbook, hojas, schemas, validation, validarAll = true) {

    let schemaJson = {};
    let repuestaOK = {};
    let repuestaError = {};
    let arrayOK = [];
    let arrayError = [];
    const arrayHoja = workbook.SheetNames;

    console.log('Hojas:', arrayHoja);

    for (let index = 0; index < schemas.length; index++) {
        const schema = schemas[index];
        let atributosJson = util.obtenerAtributosJoi(schema._ids._byKey);
        schemaJson[index] = atributosJson;
                    }
    console.log("schemaJson:", schemaJson);

    for (let index = 0; index < arrayHoja.length; index++) {
        let hojaExcel = arrayHoja[index];
        let ws = workbook.Sheets[hojaExcel];

        for (let i = 0; i < hojas.length; i++) {
            const hoja = hojas[i];
            if (hoja === hojaExcel.toUpperCase()) {
                let arrayDataJson = xlsx.utils.sheet_to_json(ws, { raw: false, header: schemaJson[i], range: 2, skipHeader: true });
                //  console.log("arrayDataJson:", arrayDataJson);
                for (let j = 0; j < arrayDataJson.length; j++) {
                    const dataJson = arrayDataJson[j];
                    let esValidoAtributosJson = util.validarAtributosJson(dataJson, schemaJson[i], validarAll);
                    if (esValidoAtributosJson) {
                        let esValidoInput = await validation[i](dataJson);
                        if (esValidoInput) {
                            arrayOK.push(dataJson);
                    } else {
                            arrayError.push(dataJson);
                    }
                } else {
                        arrayError.push(dataJson);
                        console.log('La fila del excel no tiene data que corresponda a las cabeceras establecidas para ' + hoja);
                }
            }
        }
    }
        repuestaOK[index] = arrayOK;
        repuestaError[index] = arrayError;
        arrayOK = [];
        arrayError = [];

}

    let respuesta = {
        repuestaOK: repuestaOK,
        repuestaError: repuestaError
    }

    return respuesta;
}

async function obtenerResultados(workbook, tipoPrueba) {

    let jsonRespuesta = {};
    let arrayPrueba = [];
    let arrayCuarentena = [];
    let arrayHospitalizacion = [];
    let contPrueba = 0;
    let contCuarentena = 0;
    let contHospitaliacion = 0;
    let contErrorPrueba = 0;
    let contErrorCuarentena = 0;
    let contErrorHospitaliacion = 0;

    const arrayHoja = workbook.SheetNames;
    console.log('Hojas:', arrayHoja);
    let atributosPruebaJson = util.obtenerAtributosJoi(SchemaBodyPrueba.prueba._ids._byKey);
    let atributosHospitalizacionJson = util.obtenerAtributosJoi(SchemaBodyHospitalizacion.hospitalizacion._ids._byKey);
    let atributosCuarentenaJson = util.obtenerAtributosJoi(SchemaBodyCuarentena.cuarentena._ids._byKey);

    for (let index = 0; index < arrayHoja.length; index++) {
        let hoja = arrayHoja[index];
        let ws = workbook.Sheets[hoja];

        if (constante.PRUEBA === hoja.toUpperCase()) {
            let dataJson = xlsx.utils.sheet_to_json(ws, { raw: false, header: atributosPruebaJson, range: 1, skipHeader: true });

            console.log('Ingreso a hoja Prueba');
            for (let index = 1; index < dataJson.length; index++) {
                const element = dataJson[index];
                let array = _.map(element, function(value, key) {

                    try {
                        return value.toString().trim();
                    } catch (error) {
                        console.log('Warning:', error);
                        return value;
                    }
                });

                let pruebaJson = requestPruebaToJson(array);
                let esValidoAtributosJson = util.validarAtributosJson(pruebaJson, atributosPruebaJson, false);
                if (esValidoAtributosJson) {
                    let pruebaClass = requestPruebaToClass(array);
                    let esValidoInput = await validation.addPruebaValidation(pruebaJson, tipoPrueba);

                    if (!esValidoInput) {
                        pruebaClass.tipo_documento = constante.NO_VALIDO;
                        contErrorPrueba++;
                    } else {
                        contPrueba++;
                        pruebaClass.id_tipo_prueba = pruebaJson.id_tipo_prueba;
                        pruebaClass.tipo_prueba = pruebaJson.tipo_prueba;
                        pruebaClass.id_resultado = pruebaJson.id_resultado;
                        pruebaClass.resultado = pruebaJson.resultado;
                    }

                    arrayPrueba.push(pruebaClass);
                } else {
                    console.log('La fila del excel no tiene data que corresponda a las cabeceras establecidas para Prueba');
                }
            }
            console.log('contPrueba:', contPrueba);
            console.log('contErrorPrueba:', contErrorPrueba);

        } else if (constante.CUARENTENA === hoja.toUpperCase()) {
            console.log('Ingreso a hoja Cuarentena');
            let dataJson = xlsx.utils.sheet_to_json(ws, { raw: false, header: atributosCuarentenaJson, range: 1, skipHeader: true });

            for (let index = 1; index < dataJson.length; index++) {
                const element = dataJson[index];
                let array = _.map(element, function(value, key) {
                    try {
                        return value.toString().trim();
                    } catch (error) {
                        console.log('Warning:', error);
                        return value;
                    }
                });

                let cuarentenaJson = requestCuarentenaToJson(array);
                let esValidoAtributosJson = util.validarAtributosJson(cuarentenaJson, atributosPruebaJson, false);
                if (esValidoAtributosJson) {
                    let cuarentenaClass = requestCuarentenaToClass(array);
                    let esValidoInput = await validation.addCuarentenaValidation(cuarentenaJson);

                    if (!esValidoInput) {
                        cuarentenaClass.tipo_documento = constante.NO_VALIDO;
                        contErrorCuarentena++;
                    } else {
                        contCuarentena++;
                    }
                    arrayCuarentena.push(cuarentenaClass);
                }
            }

            console.log('contCuarentena:', contCuarentena);
            console.log('contErrorCuarentena:', contErrorCuarentena);

        } else if (constante.HOSPITALIZACION === hoja.toUpperCase()) {
            let dataJson = xlsx.utils.sheet_to_json(ws, { raw: false, header: atributosHospitalizacionJson, range: 1, skipHeader: true });

            console.log('Ingreso a hoja Hospitalizacion');
            for (let index = 1; index < dataJson.length; index++) {
                const element = dataJson[index];
                let array = _.map(element, function(value, key) {
                    try {
                        return value.toString().trim();
                    } catch (error) {
                        console.log('Warning:', error);
                        return value;
                    }
                });

                let hospitalizacionJson = requestHospitalizacionToJson(array);
                let esValidoAtributosJson = util.validarAtributosJson(hospitalizacionJson, atributosHospitalizacionJson, false);
                    if (esValidoAtributosJson) {
                    let hospitalizacionClass = requestHospitalizacionToClass(array);
                    let esValidoInput = await validation.addHospitalizacionValidation(hospitalizacionJson);

                    if (!esValidoInput) {
                        hospitalizacionClass.tipo_documento = constante.NO_VALIDO;
                        contErrorHospitaliacion++;
                    } else {
                        contHospitaliacion++;
                    }
                    arrayHospitalizacion.push(hospitalizacionClass);
                } else {
                    console.log('La fila del excel no tiene data que corresponda a las cabeceras establecidas para Hospitalizacion');
                }
            }

            console.log('contHospitaliacion:', contHospitaliacion);
            console.log('contErrorHospitaliacion:', contErrorHospitaliacion);
        }

    }
    jsonRespuesta.prueba = arrayPrueba;
    jsonRespuesta.cuarentena = arrayCuarentena;
    jsonRespuesta.hospitalizacion = arrayHospitalizacion;
    return jsonRespuesta;

                }

function requestPruebaToJson(array) {

    let request = {};
    request.tipo_documento = array[0];
    request.numero_documento = array[1];
    request.tipo_prueba = array[2];
    request.fecha_programacion = excelDateToJSDate(array[3]); //array[3];
    request.fecha_realizacion = excelDateToJSDate(array[4]); //array[4];
    request.fecha_resultado = excelDateToJSDate(array[5]); //array[5];
    request.laboratorio = array[6];
    request.resultado = array[7];

    return request;
            }

function requestPruebaToClass(array) {
    let prueba = new Prueba();
    prueba.tipo_documento = array[0];
    prueba.numero_documento = array[1];
    prueba.tipo_prueba = array[2];

    try {
        prueba.fecha_programacion = excelDateToJSDate(array[3]); //util.obtenerFechaUtc(array[3]);
        prueba.fecha_realizacion = excelDateToJSDate(array[4]); //util.obtenerFechaUtc(array[4]);
        prueba.fecha_resultado = excelDateToJSDate(array[5]); //util.obtenerFechaUtc(array[5]);
        prueba.laboratorio = array[6].toUpperCase();
        prueba.resultado = array[7].toUpperCase();
        return prueba;
    } catch (error) {
        let mensaje = 'No Paso el cast de fecha Prueba Importar';
        console.log(mensaje + ' Nro documento:', prueba.numero_documento + ' ERROR:' + error);
        prueba.tipo_documento = constante.NO_VALIDO;
        return prueba;
}

}

function requestCuarentenaToJson(array) {

    let request = {};
    request.tipo_documento = array[0];
    request.numero_documento = array[1].toString();
    request.motivo = array[2];
    request.fecha_ingreso = excelDateToJSDate(array[3]); //array[3];
    request.fecha_alta = excelDateToJSDate(array[4]); //array[4];
    return request;
}

function requestCuarentenaToClass(array) {
    let cuarentena = new Cuarentena();
    cuarentena.tipo_documento = array[0];
    cuarentena.numero_documento = array[1];
    cuarentena.motivo = array[2].toUpperCase();
    cuarentena.retorno_labores = null;

    try {
        cuarentena.fecha_ingreso = excelDateToJSDate(array[3]); //util.obtenerFechaUtc(array[3]);
        cuarentena.fecha_alta = excelDateToJSDate(array[4]); //util.obtenerFechaUtc(array[4]);
    } catch (error) {
        let mensaje = 'No Paso el cast de fecha Cuarentena Importar';
        console.log(mensaje + ' Nro documento:', cuarentena.numero_documento);
        cuarentena.tipo_documento = constante.NO_VALIDO;
    }
    return cuarentena;
}

function requestHospitalizacionToJson(array) {

    let request = {};
    request.tipo_documento = array[0];
    request.numero_documento = array[1].toString();
    request.motivo = array[2];
    request.centro_medico = array[3];
    request.fecha_ingreso = excelDateToJSDate(array[4]); // array[4];
    request.fecha_alta = excelDateToJSDate(array[5]); // array[5];
    return request;
    }

function requestHospitalizacionToClass(array) {
    let hospitalizacion = new Hospitalizacion();
    hospitalizacion.tipo_documento = array[0];
    hospitalizacion.numero_documento = array[1];
    hospitalizacion.motivo = array[2].toUpperCase();
    hospitalizacion.centro_medico = array[3].toUpperCase();

    hospitalizacion.retorno_labores = null;

    try {
        hospitalizacion.fecha_ingreso = excelDateToJSDate(array[4]); // util.obtenerFechaUtc(array[4]);
        hospitalizacion.fecha_alta = excelDateToJSDate(array[5]); //  util.obtenerFechaUtc(array[5]);
    } catch (error) {
        let mensaje = 'No Paso el cast de fecha Hospitalizacion Importar';
        console.log(mensaje + ' Nro documento:', hospitalizacion.numero_documento);
        hospitalizacion.tipo_documento = constante.NO_VALIDO;
    }
    return hospitalizacion;
}


function esFormatoFechaValido(fecha) {
    return fecha.match(/(\d{4})-(\d{2})-(\d{2})/);
}

// function excelDateToJSDate(fecha) {

//     try {
//         if (esFormatoFechaValido(fecha)) {
//             return util.obtenerFechaUtc(fecha);
//         } else {

//             let num = Number(fecha);
//             if (_.isNaN(num)) {
//                 return null;
//             } else {
//                 var utc_days = Math.floor(fecha - 25569);
//                 var utc_value = utc_days * 86400;
//                 var date_info = new Date(utc_value * 1000);
//                 /*
//                     var fractional_day = fecha - Math.floor(fecha) + 0.0000001;
//                     var total_seconds = Math.floor(86400 * fractional_day);
//                     var seconds = total_seconds % 60;
//                     total_seconds -= seconds;
//                     var hours = Math.floor(total_seconds / (60 * 60));
//                     var minutes = Math.floor(total_seconds / 60) % 60;
//                 */
//                 var d = date_info.getDate() + 1;
//                 var m = date_info.getMonth() + 1; //Month from 0 to 11
//                 var y = date_info.getFullYear();

//                 let fechaCadena = '' + y + '-' + (m <= 9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
//                 console.log('fechaCadena:', fechaCadena);
//                 console.log('util.obtenerFechaUtc(fechaCadena):', util.obtenerFechaUtc(fechaCadena));
//                 return util.obtenerFechaUtc(fechaCadena);
//             }

//         }
//     } catch (error) {
//         console.log('ERROR:', error);
//         return null;
//     }


// }

function excelDateToJSDate(fecha, incluyeHora = false) {

    console.log('fecha:', fecha);
    try {
        if (esFormatoFechaValido(fecha) && !incluyeHora) {
            return util.obtenerFechaUtc(fecha);
        } else {
            console.log("convirtiendo fecha y hora:", fecha);
            let num = Number(fecha);
            //  console.log("num:", num);
            //  if (_.isNaN(num)) {
            //     return null;
            //  } else {
                var utc_days = Math.floor(fecha - 25569);
                var utc_value = utc_days * 86400;
                var date_info = new Date(utc_value * 1000);



                var d = date_info.getDate() + 1;
                var m = date_info.getMonth() + 1; //Month from 0 to 11
                var y = date_info.getFullYear();

                let fechaCadena = '' + y + '-' + (m <= 9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
                console.log('fechaCadena:', fechaCadena);

                if (incluyeHora) {
                    var fractional_day = fecha - Math.floor(fecha) + 0.0000001;
                    var total_seconds = Math.floor(86400 * fractional_day);
                    var seconds = total_seconds % 60;
                    total_seconds -= seconds;
                    var hours = Math.floor(total_seconds / (60 * 60));
                    var minutes = Math.floor(total_seconds / 60) % 60;
                fechaCadena += hours + ':' + minutes;
                    console.log('fechaCadena::', fechaCadena);
                }

                console.log('util.obtenerFechaUtc(fechaCadena):', util.obtenerFechaUtc(fechaCadena));
                return util.obtenerFechaUtc(fechaCadena);
            //  }

        }
    } catch (error) {
        console.log('ERROR:', error);
        return null;
    }


}
module.exports = {
    importarUsuarios,
    importarPruebas,
    importarExcel,
    excelDateToJSDate//,
    // guardarPDF,
    // guardarFirma,
    // guardarDocumento

}