const constante = require("../util/constante");
const azureStorage = require('azure-storage');
const { Configuracion } = require('../models/configuracion.js');
//const moment = require('moment');
const moment = require('moment-timezone');
const _ = require('lodash');

async function obtenerUsuarioFromItem(item, areaNombre, riesgoExposicionNombre, sedeNombre, tipoUsuarioNombre, tipoDocumentoNombre, unidadNombre, paisNombre, centroCostoNombre) {

    let json = {};
    json.apellido_paterno = item.apellido_paterno;
    json.apellido_materno = item.apellido_materno;
    json.nombres = item.nombres;
    json.correo_electronico = item.correo_electronico;
    json.id_tipo_documento = item.tipo_documento;
    json.tipo_documento = tipoDocumentoNombre;
    json.numero_documento = item.numero_documento;
    json.id_riesgo_exposicion = item.riesgo_exposicion;
    json.riesgo_exposicion = riesgoExposicionNombre;
    json.id_tipo_colaborador = item.tipo_usuario;
    json.tipo_colaborador = tipoUsuarioNombre;
    json.activo = true;
    json.triaje_inicial_completado = false;

    if (item.fecha_nacimiento) {
        json.fecha_nacimiento = item.fecha_nacimiento
    }

    if (item.sexo) {
        if (constante.SEXO_M === item.sexo) {
            json.sexo = constante.MASCULINO;
        } else if (constante.SEXO_F === item.sexo) {
            json.sexo = constante.FEMENINO;
        }
    }

    if (item.telefono) {
        json.telefono = item.telefono;
    }

    if (item.cargo) {
        json.cargo = item.cargo;
    }

    if (item.jefe_directo) {
        json.jefe_directo = item.jefe_directo;
    }

    let datos_compania = {};
    datos_compania.id_unidad = item.unidad;
    datos_compania.unidad = unidadNombre;
    datos_compania.id_area = item.area;
    datos_compania.area = areaNombre;
    datos_compania.id_sede = item.sede;
    datos_compania.sede = sedeNombre;

    if (item.pais) {
        datos_compania.cod_pais = item.pais;
        datos_compania.pais = paisNombre;
    }

    if (item.centro_costo) {
        datos_compania.id_centro_costo = item.centro_costo;
        datos_compania.centro_costo = centroCostoNombre;
    }

    json.datos_compania = datos_compania;

    json.fecha_creacion = obtenerFechaNowLima();
    return json;

}

async function obtenerColaboradorSubSchema(usuario) {
    let user = _.pick(usuario, ['apellido_paterno', 'apellido_materno', 'nombres', 'id_tipo_documento', 'tipo_documento', 'numero_documento', 'telefono', 'correo_electronico', 'factores_riesgo', 'datos_compania', 'riesgo_exposicion', 'triaje_inicial_completadO','resultado_sintomatologico']);
    user.id_colaborador = usuario._id;
    user.nombre_completo = user.nombres + ' ' + user.apellido_paterno + ' ' + user.apellido_materno
    return user;
}

async function obtenerJsonFromObject(object, arrayAtributos) {
    return _.pick(object, arrayAtributos);
}

async function obtenerPruebaSchema(object) {
    let arrayAtributos = [];
    return await obtenerJsonFromObject(object, arrayAtributos);
}


async function obtenerNombre(data, id) {
    try {
        for (let y = 0; y < data.length; y++) {
            const json = data[y];
            if (json.id === id) {
                return json.nombre;
            }
        }
    } catch (error) {
        console.log(error);
        return '';
    }

}

async function obtenerUsuario(data, id) {
    try {
        for (let y = 0; y < data.length; y++) {
            const json = data[y];
            if (json.id === id) {
                return json.usuario;
            }
        }
    } catch (error) {
        console.log(error);
        return false;
    }

}



function clone(obj) {
    return JSON.parse(JSON.stringify(obj));
}

function eliminarAtributosJson(jsonObj, atributos) {
    for (let index = 0; index < atributos.length; index++) {
        const element = atributos[index];
        delete jsonObj[element];
    }
    return jsonObj;

}

async function enviarMensajeToGlobalQueue(colaborador, accionTipo, compania_id) {

    // Notificar a Global de nuevo colaborador por compania
    console.log('Notificar a Global');
    // let configuracion = await Configuracion.findOne({ activo: true });

    //  if (typeof configuracion != 'undefined' && configuracion != null) {
    console.log('Creando mensaje...');
    let data = {
        accion: accionTipo,
        tipo_documento: colaborador.tipo_documento,
        numero_documento: colaborador.numero_documento,
        activo: colaborador.activo,
        triaje_inicial_completado: colaborador.triaje_inicial_completado,
        compania_id: compania_id,
        colaborador_id: '' + colaborador._id
    };
    // Envio al queue de account storage global
    console.log('Enviando mensaje...', data);

    // Instancia queue service
    var queueSvc = azureStorage.createQueueService();
    console.log('queue_name->', constante.QUEUE_SYNC)
    queueSvc.createMessage(constante.QUEUE_SYNC, Buffer.from(JSON.stringify(data)).toString('base64'), function(error, results, response) {
        if (!error) {
            console.log('Colaborador enviado!')
        } else {
            console.log('Error===>', error);
        }
    });
    //  }
}

/*
async function enviarMensajeToGlobalQueue(colaborador, accionTipo) {

    // Instancia queue service
    var queueSvc = azureStorage.createQueueService();

    // Notificar a Global de nuevo colaborador por compania
    console.log('Notificar a Global');
    let configuracion = await Configuracion.findOne({ activo: true });

    if (typeof configuracion != 'undefined' && configuracion != null) {
        console.log('Creando mensaje...');
        let data = {
            accion: accionTipo,
            tipo_documento: colaborador.tipo_documento,
            numero_documento: colaborador.numero_documento,
            activo: colaborador.activo,
            triaje_inicial_completado: colaborador.triaje_inicial_completado,
            compania_id: configuracion.compania_id,
            colaborador_id: '' + colaborador._id
        };
        // Envio al queue de account storage global
        console.log('Enviando mensaje...', data);
        console.log('queue_name->', constante.QUEUE_SYNC)
        queueSvc.createMessage(constante.QUEUE_SYNC, Buffer.from(JSON.stringify(data)).toString('base64'), function(error, results, response) {
            if (!error) {
                console.log('Colaborador enviado!')
            } else {
                console.log('Error===>', error);
            }
        });
    }
}

*/
function removerElementArray(array, index) {
    delete array[index];
}

function removerObjectArray(array, id) {
    return _.remove(array, function(obj) {
        return obj._id === id;
    });
}

function existeValorArrarObject(array, attribute, valor) {
    let index = _.findIndex(array, function(o) {
        let nombre = o[attribute].toUpperCase().trim();
        let valorUpper = valor.toUpperCase().trim();

        let nombreNormalizado = normalizarTexto(nombre);
        let valorNormalizado = normalizarTexto(valorUpper);

        return (nombre == valorUpper) || (nombreNormalizado == valorNormalizado);
    });

    let json = {};
    json.index = index;
    json.existe = index > -1 ? true : false;
    return json;
}

function extraerObjectDesdeArray(array, paramJson) {
    return _.find(array, _.matches(paramJson));
}

function normalizarTexto(texto) {

    return texto.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

function trimObjectJson(object) {
    try {
        for (var attributename in object) {
            object[attributename] = object[attributename].trim();
        }
    } catch (error) {
        console.log('Error trimObjectJson:', error);
    }

}

function esVacio(object) {
    return _.isEmpty(object);
}

function esDate(fecha) {
    return _.isDate(fecha);
}

function esVacioParams(params, condicional = constante.CONDICIONAL_AND) {
    for (let index = 0; index < params.length; index++) {
        const element = params[index];
        if (constante.CONDICIONAL_AND == condicional) {
            if (esVacio(element)) {
                return true;
            }
        } else if (constante.CONDICIONAL_OR == condicional) {
            if (!esVacio(element)) {
                return false;
            }
        }
    }

    return constante.CONDICIONAL_AND == condicional ? false : true;
}

function validarAtributosJson(jsonObj, atributos, validarAll = true) {

    let cont = 0;

    for (let index = 0; index < atributos.length; index++) {
        let element = atributos[index];
        if (Object.prototype.hasOwnProperty.call(jsonObj, element)) {
            cont++;
        }
    }

    if (validarAll) {
        if (atributos.length == cont) {
            return true;
        }
    } else {
        if (cont > 0) {
            return true;
        }
    }
    return false;
}

function obtenerAtributosJoi(jsonObject) {
    let arrayAtributo = [];
    for (var entry of jsonObject.entries()) {
        var key = entry[0];
        arrayAtributo.push(key);
    }
    return arrayAtributo;
}

function _concat(object1, object2) {
    return _.concat(object1, object2);
}

function extraerArray(array, start, end = 0) {

    if (end == 0) {
        return _.slice(array, start);
    } else {
        return _.slice(array, start, end);
    }
}
// ----- Fechas ------

function obtenerFechaNowLima() {
    let now = moment.utc().tz("America/Lima").format('YYYY-MM-DD HH:mm:ss');
    return now;
}

function obtenerFechaNowFormat(format) {
    // 'YYYY-MM-DD'
    let now = moment.utc().tz("America/Lima").format(format);
    return now;
}

function obtenerFechaPeru(fecha){
    let now = moment(fecha).add(5,'hours');
    return now;
}

function sumarDias(startdate, format, dias) {
    let new_date = moment(startdate, format).add(dias, 'days')._d;
    return new_date;
}

function sumarDiasFecha(date, dias) {
    let new_date = moment(date).add(dias, 'days')._d;
    return new_date;
}

function obtenerFechaNowLimaDate() {
    let now = moment.utc(obtenerFechaNowLima())._d;
    return now;
}

function obtenerFechaNowSistema() {
    let now = moment.utc()._d;
    return now;
}

function obtenerFechaUtc(fecha) {

    if (!_.isEmpty(fecha)) {
        let fechaUtc = moment.utc(fecha)._d;
        return fechaUtc;
    } else {
        return null;
    }
}

function quitarEspacioEnBlancoFechaHora(fecha) {
    let array = fecha.split(' ');

    return array[0] + ' ' + array[array.length - 1];
}

function obtenerFechaUtcFormat(fecha) {
    let fechaUtc = moment(fecha).format('YYYY-MM-DD HH:mm');
    return fechaUtc;
}

function isBetweenDate(fecha, from, to, cast = true) {

    let fecha_comparar;
    let fecha_from;
    let fecha_to;
    if (cast) {
        fecha_comparar = moment.utc(fecha)._d;
        fecha_from = moment.utc(from)._d;
        fecha_to = moment.utc(to)._d;
    } else {
        fecha_comparar = fecha;
        fecha_from = from;
        fecha_to = to;
    }

    let mayorIgual = moment(fecha_comparar).isSameOrAfter(fecha_from);
    let menorIgual = moment(fecha_comparar).isSameOrBefore(fecha_to);

    return mayorIgual && menorIgual;
}

function removerHoraDeFecha(fecha) {
    return moment.utc(fecha).set({ hour: 0, minute: 0, second: 0, millisecond: 0 })._d;
}



module.exports = {
    obtenerUsuarioFromItem,
    obtenerColaboradorSubSchema,
    obtenerNombre,
    obtenerUsuario,
    clone,
    eliminarAtributosJson,
    enviarMensajeToGlobalQueue,
    removerElementArray,
    removerObjectArray,
    existeValorArrarObject,
    extraerObjectDesdeArray,
    trimObjectJson,
    obtenerAtributosJoi,
    validarAtributosJson,
    _concat,
    extraerArray,
    isBetweenDate,
    obtenerFechaNowLima,
    obtenerFechaUtc,
    obtenerFechaUtcFormat,
    obtenerFechaNowLimaDate,
    obtenerFechaNowSistema,
    obtenerFechaNowFormat,
    obtenerFechaPeru,
    sumarDias,
    sumarDiasFecha,
    removerHoraDeFecha,
    esVacio,
    esVacioParams,
    quitarEspacioEnBlancoFechaHora,
    esDate
}