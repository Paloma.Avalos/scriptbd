const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const random = require('random');
const _ = require('lodash');
const jwt_decode = require('jwt-decode');
const constante = require('../util/constante');
const { ResourceNotFoundError, InternalError } = require("../error/BusinessError");

const tokenInvalido = {
    headers: { 'Content-Type': 'application/json' },
    status: 401,
    body: { respuesta: 'El token no es valido' }
}

async function generarPassword() {
    let passRandom;
    let passEncryptado;

    try {
        passRandom = random.int(100000, 999999).toString();
        console.log("pass Random:", passRandom);
        passEncryptado = await bcrypt.hash(passRandom, constante.BCRYPT_SALT_ROUNDS);
    } catch (error) {
        passEncryptado = null;
        console.log("ERROR en la encriptacion de password:", error);
    }

    let rpta = {
        passRandom,
        passEncryptado
    }

    return rpta;
}

function validarToken(req) {
    try {
    //     let token = req.headers[constante.X_AUTH_TOKEN];
    //     token = token.replace('Bearer ', '');
    //     console.log('token:', token);
    //     console.log('constante.JWT_PRIVATE_KEY:', constante.JWT_PRIVATE_KEY);
    //     //let tokenData = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZWQwOWY2NWVjNTMwZDI1YjRmYzY3ZGUiLCJpYXQiOjE1OTA3NzMxNTB9.wxF89-nUPxUpeJrk9zT3DlflT0DvNNUOpYwbxb4Z2BI";
    //     let tokenData = jwt.verify(token, String(constante.JWT_PRIVATE_KEY));        
    //     console.log('tokenData:', tokenData);
    //     return tokenData;
    // } catch (error) {
    //     console.log('Error:', error);
    //     throw new InternalError('Hubo un problema al validar el token de acceso', req);
    // }
    let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZWQwOWY2NWVjNTMwZDI1YjRmYzY3ZGUiLCJpYXQiOjE1OTA3NzMxNTB9.wxF89-nUPxUpeJrk9zT3DlflT0DvNNUOpYwbxb4Z2BI";
    //req.headers[constante.X_AUTH_TOKEN];
        token = token.replace('Bearer ', '');
    let tokenData = jwt.verify(token, String(constante.JWT_PRIVATE_KEY));
        return tokenData;
    } catch (error) {
        throw new InternalError('Hubo un problema al validar el token de acceso', req);
    }

}

function validarTokenConClave(req, JWT_PRIVATE_KEY_DEVICE) {
    try {
        let tokenData;
        let token = req.headers[constante.X_AUTH_TOKEN];
        token = token.replace('Bearer ', '');
        if (!_.isEmpty(JWT_PRIVATE_KEY_DEVICE)) {
            console.log('Si existe la variable JWT_PRIVATE_KEY_DEVICE');
            tokenData = jwt.verify(token, JWT_PRIVATE_KEY_DEVICE);
        } else {
            console.log('No existe la variable JWT_PRIVATE_KEY_DEVICE');
            tokenData = jwt_decode(token);
        }
        console.log('tokenData:', tokenData);

        return tokenData._id;
    } catch (error) {
        console.log("ERROR validarToken:", error);
        return null;
    }

}

function generarTokenMetabase(payload, key) {

    try {
        return jwt.sign(payload, key);
    } catch (error) {
        throw new InternalError('Hubo un error al generar el token Metabase');
    }
}

module.exports = {
    generarPassword,
    validarToken,
    validarTokenConClave,
    tokenInvalido,
    generarTokenMetabase


}