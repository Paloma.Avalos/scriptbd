//const FORMATO_FECHA = 'YYYY-MM-DDTHH:mm:ss.SSS';



const PRUEBA = 'PRUEBA';
const CUARENTENA = 'CUARENTENA';
const HOSPITALIZACION = 'HOSPITALIZACION';

const IMPORTAR_PRUEBAS_HOJAS = [PRUEBA, CUARENTENA, HOSPITALIZACION];
const TEMPERATURA = 'TEMPERATURA';

const IMPORTAR_TEMPERATURA_HOJAS = [TEMPERATURA];
const IMPORTAR_USUARIO_HOJAS = ["USUARIO"];


const TIPO_PRUEBA_PCR = 'PCR';
const TIPO_PRUEBA_RAPIDA = 'RÁPIDA';
const OBSERVACION_RESULTADO_CONFIRMADO = 'Se registró el resultado positiva de una prueba COVID';
const NOTA_CAMBIO_SITUACION = 'Cambio de situación de $1 a $2: ';
const NOTA_NUEVA_SITUACION = 'Cambio de situación a $1: ';
const ULTIMOS_SINTOMAS = 'ULTIMOS_SINTOMAS';

const MSJ_ENVIO_EXITO = 'Se ha enviado un correo electrónico con tu constraseña a la cuenta ';
const MSJ_ENVIO_ERROR = 'No se encontró una cuenta de correo electrónico registrada con este nro de documento. Comunicate con RRHH para registrar tu correo electrónico ';

const BCRYPT_SALT_ROUNDS = 7;
const X_AUTH_TOKEN = 'x-auth-token';
const CON_SINTOMAS = 'CON_SINTOMAS';
const SIN_SINTOMAS = 'SIN_SINTOMAS';
const SOSPECHOSO = 'SOSPECHOSO';

const MASCULINO = 'Masculino';
const FEMENINO = 'Femenino';
const SEXO_M = 'M';
const SEXO_F = 'F';
const CANTIDAD_CAMPOS_USUARIO_IMPORTAR = 18;
const TEMPERATURA_CASO = 37.5;

//const DB_CONN_STR_DIRECT = 'mongodb+srv://healthcaredev:Admin123@clustercompanya-9vbux.azure.mongodb.net/healthcare?authSource=admin&replicaSet=ClusterCompanyA-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true';
//const DB_CONN_STR_DIRECT = 'mongodb://localhost:27017/healthcare-demo';
const DB_CONN_STR_DIRECT = 'mongodb+srv://healthcaredev:Admin123@clustercompanya-9vbux.azure.mongodb.net/healthcare-cte?authSource=admin&replicaSet=ClusterCompanyA-shard-0&readPreference=primary&ssl=true';
const MY_CONNECTION = process.env["DB_CONN_STR"] || DB_CONN_STR_DIRECT;
const MONGOOSE_DEBUG = false;

const JWT_PRIVATE_KEY_DEVICE = process.env["JWT_PRIVATE_KEY_DEVICE"] || null;
const JWT_PRIVATE_KEY = process.env["JWT_PRIVATE_KEY"] || '604d427f3b0df9bd16d79d357716c5e637201c9df012d6bfce8e8bab42cc8bb079bcea2cbc4eac34ad89cfca2bcc8ae3e84b043bf7a78d33eca58bcf049c4d48';
//const JWT_PRIVATE_KEY = process.env["JWT_PRIVATE_KEY"] || 'SEMILLA-HEALTH-CARE';

const HOST = process.env["HOST_API"] || 'https://healthcare-datoslaborales.azurewebsites.net';
const QUEUE_SYNC = process.env["QUEUE_SYNC"] || 'queueSync';


//constantes de blob storage
const ACCOUNT_ASSETS = process.env["ACCOUNT_ASSETS"] || null;
const ACCOUNT_KEY_ASSETS = process.env["ACCOUNT_KEY_ASSETS"] || null;
const CONTAINER_PRUEBAS = process.env["CONTAINER_PRUEBAS"] || "pruebas";
const CONTAINER_FIRMAS = process.env["CONTAINER_FIRMAS"] || "firmas";
const CONTAINER_DOCUMENTOS = process.env["CONTAINER_DOCUMENTOS"] || "documentos";

const URI_AREA = HOST + "/api/AreaObtener";
const URI_RIESGO = HOST + "/api/RiesgoObtener";
const URI_SEDE = HOST + "/api/SedeObtener";
const URI_TIPO_USUARIO = HOST + "/api/TipoUsuarioObtener";
const URI_TIPO_DOCUMENTO = HOST + "/api/TipoDocumentoObtener";
const URI_UNIDAD = HOST + "/api/UnidadObtener";
const URI_PAIS = HOST + "/api/PaisObtener";
const URI_CENTRO_COSTO = HOST + "/api/CentroCostoObtener";
const URI_TIPO_PRUEBA = HOST + "/api/TipoPruebaObtener";
const URI_ENVIO_CORREO = "https://temposalud.com/tempo_covit/api/1/user.php?action=send_password";
const URI_ENVIO_CORREO_NUMDOC = "https://temposalud.com/tempo_covit/api/2/user.php?action=send_employee_password";
const URI_TIPO_NOTIFICACION = HOST + "/api/TipoNotificacionObtener";
const METABASE_SITE_URL = "https://reporting.temposalud.com"
const METABASE_DASHBOARD = 11;

const CONFIRMADO = 'CONFIRMADO';


const TIPO_CARGA_PRUEBA = {
    PENDIENTE: "PENDIENTE",
    REALIZADA: "REALIZADA"
}

const TIPO_ERROR = {
    NEGOCIO: "NEGOCIO",
    TECNICO: "TECNICO"
}

const ACCION_TIPO_NUEVO = 'new';
const ACCION_TIPO_ACTUALIZAR = 'update';

const NO_VALIDO = 'NO_VALIDO';
const __EMPTY = '__EMPTY';

const TEMPERATURA_REQUEST_FORMATO_FECHA = 'YYYY-MM-DDTHH:mm';
const TEMPERATURA_REQUEST_QUERY_FORMATO_FECHA = 'YYYY-MM-DD';

const TIPO_CUESTIONARIO = {
    DIARIO: 'DIARIO',
    INICIAL: 'INICIAL'
};

const TRIAJE_SECCION_SINTOMATOLOGIA = 1;
const TRIAJE_SECCION_SITUACION_RIESGO = 2;
const TIPO_PREGUNTA = {
    SELECCION_MULTIPLE: 'SELECCION_MULTIPLE',
    SELECCION_SIMPLE_DESPLEGABLE: 'SELECCION_SIMPLE_DESPLEGABLE',
    SI_NO: 'SI_NO',
    TEXTO: 'TEXTO'
};

const PREGUNTA_OTROS = 'Otros';
const URI_RESULTADO_TRIAJE_DIARIO = process.env["URI_RESULTADO_TRIAJE_DIARIO"] || 'http://localhost:7071/api/ObtenerResultadoTriajeDiario';
const URI_CASO_CREAR = '/api/CasoCrear';

const CONDICIONAL_AND = 'AND';
const CONDICIONAL_OR = 'OR';
const ESTADO_DESACTIVO= 'Desactivo';

const MOTIVO_CASO = {
    MANUAL: 'MANUAL',
    SOSPECHOSO: 'SOSPECHOSO',
    CONTACTO_COVID: 'CONTACTO COVID',
    POSITIVO_COVID: 'POSITIVO COVID',
    CONFIRMADO: 'CONFIRMADO',
    EXPUESTO_COVID: 'EXPUESTO COVID'
};

const SITUACION_CASO = {
    NORMAL: 'NORMAL',
    EN_SEGUIMIENTO: 'En Seguimiento',
    CONFIRMADO: 'CONFIRMADO',
    EXPUESTO: 'EXPUESTO',
    FALLECIDO: 'FALLECIDO',
    AISL_HOSPITALARIO: 'AISL. HOSPITALARIO',
    AISL_DOMICILIARIO: 'AISL. DOMICILIARIO',
    ALTA_EPID: 'ALTA EPID.'
};

const TOKEN_HARD = "NO_VALIDA_d3e8838e2d08b3a04ae2d8507dd41fa4e4fa16ef3dc06d727bd1a5359a0abf7ba46caf9e5cfce307267fcb1f6d0080cee6b0ff25395c9679fd805a9a917533c3";

const TIPO_IMPORTACION = {
    PRUEBA: 'PRUEBA',
    TEMPERATURA: 'TEMPERATURA'
}

const RESULTADO_PRUEBA = {
    CONFIRMADO: 'CONFIRMADO',
    IGM_IGG_POSITIVAS: 'IGM E IGG POSITIVAS',
    IGG_POSITIVA: 'IGG POSITIVA',
    IGM_POSITIVO: 'IGM POSITIVO',
}

const CASO_TEST_UPDATE = "CASO_TEST_UPDATE";

module.exports = {
    PRUEBA,
    CUARENTENA,
    HOSPITALIZACION,
    IMPORTAR_PRUEBAS_HOJAS,
    IMPORTAR_TEMPERATURA_HOJAS,
    IMPORTAR_USUARIO_HOJAS,
    TIPO_PRUEBA_PCR,
    OBSERVACION_RESULTADO_CONFIRMADO,
    NOTA_CAMBIO_SITUACION,
    NOTA_NUEVA_SITUACION,
    ULTIMOS_SINTOMAS,
    JWT_PRIVATE_KEY_DEVICE,
    JWT_PRIVATE_KEY,
    BCRYPT_SALT_ROUNDS,
    X_AUTH_TOKEN,
    CON_SINTOMAS,
    SIN_SINTOMAS,
    SOSPECHOSO,
    MY_CONNECTION,
    MONGOOSE_DEBUG,
    CANTIDAD_CAMPOS_USUARIO_IMPORTAR,
    TEMPERATURA_CASO,
    URI_AREA,
    URI_RIESGO,
    URI_SEDE,
    URI_TIPO_USUARIO,
    URI_TIPO_DOCUMENTO,
    URI_UNIDAD,
    URI_PAIS,
    URI_CENTRO_COSTO,
    URI_TIPO_PRUEBA,
    URI_ENVIO_CORREO,
    URI_ENVIO_CORREO_NUMDOC,
    URI_TIPO_NOTIFICACION,
    MASCULINO,
    FEMENINO,
    SEXO_M,
    SEXO_F,
    TIPO_CARGA_PRUEBA,
    TIPO_ERROR,
    ACCION_TIPO_NUEVO,
    ACCION_TIPO_ACTUALIZAR,
    QUEUE_SYNC,
    CONFIRMADO,
    NO_VALIDO,
    __EMPTY,
    METABASE_SITE_URL,
    METABASE_DASHBOARD,
    TEMPERATURA_REQUEST_FORMATO_FECHA,
    TEMPERATURA_REQUEST_QUERY_FORMATO_FECHA,
    TIPO_CUESTIONARIO,
    TRIAJE_SECCION_SINTOMATOLOGIA,
    TRIAJE_SECCION_SITUACION_RIESGO,
    TIPO_PREGUNTA,
    PREGUNTA_OTROS,
    URI_RESULTADO_TRIAJE_DIARIO,
    CONDICIONAL_AND,
    CONDICIONAL_OR,
    MOTIVO_CASO,
    SITUACION_CASO,
    TOKEN_HARD,
    URI_CASO_CREAR,
    TIPO_IMPORTACION,
    TEMPERATURA,
    RESULTADO_PRUEBA,
    TIPO_PRUEBA_RAPIDA,
    ACCOUNT_ASSETS,
    ACCOUNT_KEY_ASSETS,
    ESTADO_DESACTIVO,
    MSJ_ENVIO_EXITO,
    MSJ_ENVIO_ERROR,
    CONTAINER_PRUEBAS,
    CONTAINER_FIRMAS,
    CASO_TEST_UPDATE,
    CONTAINER_DOCUMENTOS

}