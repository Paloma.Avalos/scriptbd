const constante = require("./constante");
const util = require('./util');
const { UserDefaultDb } = require('../database/UserDefaultDb');
const { Nota } = require('../models/nota.js');
const api = require('../util/apiRest');

function esResultadoConfirmado(tipo_prueba, resultado) {
    if (constante.TIPO_PRUEBA_PCR == tipo_prueba) {
        if (constante.RESULTADO_PRUEBA.CONFIRMADO == resultado) {
            return true;
        }
    } else if (constante.TIPO_PRUEBA_RAPIDA == tipo_prueba) {
        if (constante.RESULTADO_PRUEBA.IGM_IGG_POSITIVAS == resultado ||
            constante.RESULTADO_PRUEBA.IGG_POSITIVA == resultado ||
            constante.RESULTADO_PRUEBA.IGM_POSITIVO == resultado) {
            return true;
        }
    }


    return false;
}

async function validarCasoConfirmado(tipo_prueba, resultado, idToken, user, uriCrearCaso, token, casoVacio = false) {
    tipo_prueba = tipo_prueba.toUpperCase();
    resultado = resultado.toUpperCase();
    if (esResultadoConfirmado(tipo_prueba, resultado)) {
        console.log('RESULTADO CONFIRMADO');
        let arraySituacionMedico = [];
        let situacionMedico = {};

        idToken= '5f73a3a31bfaae0a345b88ff';

        try {
            let userMedico = await UserDefaultDb.buscarUsuarioPorId(idToken);

            situacionMedico.condiciones = 'Confirmado';
            situacionMedico.id_condicion = "7";
            situacionMedico.nota = constante.OBSERVACION_RESULTADO_CONFIRMADO;
            situacionMedico.fecha_registro = util.obtenerFechaNowLimaDate();
            situacionMedico.id_medico = idToken;
            situacionMedico.nombre_medico = userMedico.nombres + ' ' + userMedico.apellido_materno;

            arraySituacionMedico.push(situacionMedico);
            user.resultado_medico = util._concat(user.resultado_medico, arraySituacionMedico);

            // region nota
            let nota = new Nota({
                id_colaborador: user._id,
                observacion: constante.OBSERVACION_RESULTADO_CONFIRMADO,
                medico: {
                    id_medico: userMedico._id,
                    nombre: userMedico.nombres,
                    apellido_paterno: userMedico.apellido_paterno,
                    apellido_materno: userMedico.apellido_materno,
                },
                fecha_creacion: util.obtenerFechaNowLimaDate(),
                activo: true
            });
            let notaGenerada = await nota.save();
            console.log('Se ha creado la nota:', notaGenerada._id);
        } catch (error) {
            console.log('Error al crear la nota para la prueba que dio positivo con el id:', idToken + ' ERROR:' + error);
        }

        if (casoVacio) {
            console.log('Se consume el servicio ' + uriCrearCaso + ' para crear un caso automatico');
            let userID = '5f73a3a31bfaae0a345b88ff';
            let casoGenerado = await generarCaso(userID/*user._id*/, token, uriCrearCaso);
            console.log("casoGenerado:", casoGenerado);
            if (!util.esVacio(casoGenerado.data._id)) {
                let rpta = {
                    id_caso: casoGenerado.data._id
                }
                return rpta;
            }


        } else {
            console.log("Existe un caso para el colaborador _id:", user._id);
        }

    } else {
        // console.log('RESULTADO NO CONFIRMADO');
    }

    return false;
}



async function generarCaso(idColaborador, token, uriCrearCaso) {
    token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZWQwOWY2NWVjNTMwZDI1YjRmYzY3ZGUiLCJpYXQiOjE1OTA3NzMxNTB9.wxF89-nUPxUpeJrk9zT3DlflT0DvNNUOpYwbxb4Z2BI";
    try {
        let requestCrearCaso = {
            id_colaborador: idColaborador,
            fecha_inicio_caso: util.obtenerFechaNowSistema(),
            token: token,
            motivo: constante.MOTIVO_CASO.CONFIRMADO
        }

        console.log('requestCrearCaso:', JSON.stringify(requestCrearCaso));

        let caso = await api.postCrearCaso(uriCrearCaso, requestCrearCaso);

        console.log('call Caso:', caso);
        return caso;
    } catch (error) {
        console.log('Error al crear caso desde guardar triaje diario:', error);
    }
}


async function generarCasoSospecha(idColaborador, motivo, token, uriCrearCaso) {
    try {
        let requestCrearCaso = {
            id_colaborador: idColaborador,
            fecha_inicio_caso: util.obtenerFechaNowSistema(),
            token: token,
            motivo: motivo
        }

        console.log('requestCrearCaso:', JSON.stringify(requestCrearCaso));

        let caso = await api.postCrearCaso(uriCrearCaso, requestCrearCaso);

        console.log('call Caso:', caso);
        return caso;
    } catch (error) {
        console.log('Error al crear caso desde guardar triaje diario:', error);
    }
}

module.exports = {
    validarCasoConfirmado,
    generarCasoSospecha
    // esResultadoConfirmado,

}