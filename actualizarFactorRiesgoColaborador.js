const mongoose = require('mongoose');
const path = require('path');
const fs = require('fs');
const CSVToJSON = require('csvtojson');
const { ColaboradorSubSchema } = require('./models/subDocument/colaborador');
const { Test } = require('./models/test');
const _ = require('lodash');


async function main() {
    console.log("Iniciando proceso de validación de Usuarios");

    await mongoose.connect("mongodb+srv://healthcaredev:Admin123@clustercompanya-9vbux.azure.mongodb.net/healthcare-cte?authSource=admin&replicaSet=ClusterCompanyA-shard-0&readPreference=primary&ssl=true", {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    const testColaborador = await CSVToJSON().fromFile('./OutputCsv/colaboradoFRFinal.csv');
    for (let i = 0; i < testColaborador.length; i++) {
        let colaborador = await Test.find({ id_colaborador: testColaborador[i]["objectId"] }).lean().sort({ fecha_creacion: 'desc' });
        console.log(colaborador);
        if (colaborador) {

            for (let j = 0; j < colaborador.length; j++) {
                if (!_.isUndefined(colaborador[j].colaborador.factores_riesgo)) {
                    console.log("Factores de riesgo:  ", colaborador[j].colaborador.factores_riesgo);
                    let colaboradorSubSchema = await obtenerColaboradorSubSchema(colaborador[j].colaborador.factores_riesgo);
                    console.log("resultado de colaboradorSubSchema :  ", colaboradorSubSchema);
                }
            }
        }
    }
}


async function obtenerColaboradorSubSchema(usuario) {
    console.log("Ingresa a obtenerColaboradorSubSchema: ", usuario);
    let user = _.pick(usuario, ['factores_riesgo']);

    user.id_colaborador = usuario._id;
    user.factores_riesgo = user.factores_riesgo;
    return user;

}

main()
    .then(v => console.log("Se termino la actualización"))
    .catch(err => console.error(err));