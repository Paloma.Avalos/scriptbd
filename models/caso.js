const mongoose = require('mongoose');

const { ColaboradorSubSchema } = require('./subDocument/colaborador');
const constante = require("../util/constante");
const { TriajeSubSchema } = require('./subDocument/triaje');
var Schema = mongoose.Schema;

const casoSchema = new mongoose.Schema({

    id_colaborador: mongoose.Types.ObjectId,
    colaborador: {
        type: ColaboradorSubSchema
    },
    caso_gruporiesgo: {
        type: Boolean,
        default: false
    },
    fecha_creacion: { type: Date, default: Date.now },
    fecha_inicio_caso: { type: Date, default: Date.now },
    fecha_atencion: { type: Date },
    fecha_alta_epid:{type:Date},
    fecha_alta_medica: { type: Date },
    caso_alta_epid:{
        type:Boolean,
        default:false
    },
    caso_tipo: String,
    caso_situacion: {
        type: String,
        default: "En seguimiento"
    },
    caso_atencion: {
        type: String
    },
    caso_finalizado: {
        type: Boolean,
        default: false
    },
    status: {
        type: Boolean,
        default: true
    },
    test: {
        type: TriajeSubSchema
            // required: true
    },
    id_test_diario: { type: Schema.ObjectId, ref: "test" }



})

casoSchema.set('collection', 'caso');
const Caso = mongoose.model('caso', casoSchema, 'caso');


exports.Caso = Caso;