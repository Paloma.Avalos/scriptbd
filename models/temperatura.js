const mongoose = require('mongoose');

/**
 * Nombre del modelo Colaborador de mongoose
 */
const COLABORADOR_MODEL_NAME = 'usuario';

/**
 * Nombre de colección Temperatura
 */
const COLLECTION_NAME = 'temperatura';

/**
 * Nombre de colección Temperatura
 */
const MODEL_NAME = 'temperatura';

/**
 * Schema para colección temperatura
 */
const temperaturaSchema = new mongoose.Schema({
    colaborador_id: {
        type: mongoose.Types.ObjectId,
        required: true,
        ref: COLABORADOR_MODEL_NAME
    },
    fecha_registro: {
        type: Date,
        required: true
    },
    temperatura: {
        type: Number,
        required: true
    },
    creado_por_id: {
        type: mongoose.Types.ObjectId,
        required: true,
        ref: COLABORADOR_MODEL_NAME
    },
    fecha_creacion: {
        type: Date,
        required: true
    },
    id_caso: mongoose.Types.ObjectId,
    activo: {
        type: Boolean,
        required: true
    }
}, { collection: COLLECTION_NAME });

/**
 * Modelo Temperatura
 */
const temperaturaModel = mongoose.model(MODEL_NAME, temperaturaSchema);

module.exports.Temperatura = temperaturaModel;