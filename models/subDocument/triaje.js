const { object } = require('@hapi/joi');
const mongoose = require('mongoose');

const resultadoEvaluacion = {
    llave: String,
    valor: String
};

const TriajeSubSchema = new mongoose.Schema({
    id_test: mongoose.Types.ObjectId,
    resultado_evaluacion: resultadoEvaluacion,
    resultado_sintomatologico: resultadoEvaluacion,
    numero_sintomas: Number,
    resultado_cuestionario: Array,
    
}, { _id: false })

exports.TriajeSubSchema = TriajeSubSchema;