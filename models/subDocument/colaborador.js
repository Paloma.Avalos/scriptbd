const mongoose = require('mongoose');
var mongoosePaginate = require("mongoose-paginate-v2");
var aggregatePaginate = require("mongoose-aggregate-paginate-v2");
var Schema = mongoose.Schema;


const ColaboradorSubSchema = new mongoose.Schema({
    id_colaborador: mongoose.Types.ObjectId,
    apellido_paterno: {
        type: String,
        required: true
    },
    apellido_materno: {
        type: String,
        required: true
    },
    nombres: {
        type: String,
        required: true
    },
    nombre_completo: {
        type: String
    },
    activo:{
        type: Boolean, default: true
    },
    triaje_inicial_completado:{
        type: Boolean, default: true
    },
    resultado_sintomatologico: {
        llave: {
            type: String
        },
        valor: {
            type: String
        },
        orden: {
            type: Number
        }      
    },
    //resultado_medico:Array,
    resultado_medico: [{
        condiciones: String,
        id_condicion: String,
        nota: String,
        fecha_registro: {
            type: Date,
            default: Date.now
        },
        id_medico: String,
        nombre_medico: String
    }],
    seguro_atencion_medica: {
        type: String
    },
    id_tipo_documento: Number,
    tipo_documento: {
        type: String
    },
    numero_documento: {
        type: String
    },
    telefono: {
        type: String
    },
    correo_electronico: {
        type: String,
        required: true
    },
    factores_riesgo: Array,
    riesgo_exposicion:String,
    datos_compania: {
        id_pais: Number,
        cod_pais: String,
        pais: String,
        id_grupo: Number,
        nombre_grupo: String,
        compania: String,
        id_area: {
            type: Number,
            required: true
        },
        area: {
            type: String,
            required: true
        },
        id_unidad: {
            type: Number,
            required: true
        },
        unidad: {
            type: String,
            required: true
        },
        id_sede: {
            type: Number,
            required: true
        },
        sede: {
            type: String,
            required: true
        },
        cargo: String,
        id_centro_costo: Number,
        centro_costo: String,
        subarea: String
    }
}, { _id: false });


ColaboradorSubSchema.plugin(mongoosePaginate); //second step
ColaboradorSubSchema.plugin(aggregatePaginate); //second step
ColaboradorSubSchema.set('collection', 'ColaboradorSubSchema');

const Colaborador = mongoose.model('colaborador', ColaboradorSubSchema,'colaborador');
exports.Colaborador = Colaborador;