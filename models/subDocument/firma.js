const mongoose = require('mongoose');


const FirmaSubSchema= new mongoose.Schema({
    url_firma:{type:String},
    fecha_creacion:{
        type:Date,
        default:Date.now
    },
    status:{
        type:Boolean,
        default:true
    }
})


exports.firmaSubSchema=FirmaSubSchema;