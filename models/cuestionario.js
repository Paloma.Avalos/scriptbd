const mongoose = require('mongoose');

/**
 * Nombre de colección Cuestionario
 */
const COLLECTION_NAME = 'cuestionario';

/**
 * Nombre de colección Cuestionario
 */
const MODEL_NAME = 'cuestionario';

/**
 * Schema para cuadro_texto
 */
const cuadroTextoSchema = {
    etiqueta: String
};

/**
 * Schema para data_pregunta
 */
const dataPreguntaSchema = {
    etiqueta: String,
    valor: String
};

/**
 * Schema para paso
 */
const pasoSchema = {
    numero_paso: Number,
    pregunta: String,
    texto_apoyo: String,
    id_tipo_pregunta: Number,
    tipo_pregunta: String,
    requerido: Boolean,
    data_pregunta: [dataPreguntaSchema],
    cuadro_texto: cuadroTextoSchema,
    tipo_data_colaborador: String,
    valor: String
};

/**
 * Schema para secciones
 */
const seccionSchema = {
    numero_seccion: Number,
    nombre_seccion: String,
    titulo_seccion: String,
    numero_pasos: Number,
    pasos: [pasoSchema]
};

/**
 * Schema para colección Cuestionario
 */
const cuestionarioSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    tipo_cuestionario: {
        type: String,
        required: true
    },
    activo: {
        type: Boolean,
        required: true
    },
    fecha_creacion: {
        type: Date,
        required: true
    },
    num_secciones: {
        type: Number,
        required: true
    },
    criterio_sospecha: Array,
    condicion_sospecha: {
        and: Array,
        or: [Array]
    },
    secciones: [seccionSchema]
}, { collection: COLLECTION_NAME });

/**
 * Modelo Cuestionario
 */
const cuestionarioModel = mongoose.model(MODEL_NAME, cuestionarioSchema);

module.exports.Cuestionario = cuestionarioModel;
