const mongoose = require('mongoose');
var Schema = mongoose.Schema;
const { ColaboradorSubSchema } = require('./subDocument/colaborador');

const testSchema = new mongoose.Schema({
    id_colaborador: { type: Schema.ObjectId, ref: "usuario" },
    colaborador:{
        type:ColaboradorSubSchema
    },
    resultado_cuestionario: Array,
    fecha_creacion: Date,
    resultado_evaluacion: Object,
    id_cuestionario: { type: Schema.ObjectId, ref: "cuestionario" },
    resultado_sintomatologico: Object,
    numero_sintomas: Number,
    fecha_modificacion: Date

});

testSchema.set('collection', 'test');
const Test = mongoose.model('test', testSchema);

exports.Test = Test;