const mongoose = require('mongoose');
const { ColaboradorSubSchema } = require('./subDocument/colaborador');
const {NotaSchema} = require('./nota');
var Schema = mongoose.Schema;

const medicoSchema = {
    id_medico: {type: Schema.ObjectId, ref: "usuario"},
    nombre: String,
    apellido_paterno: String,
    apellido_materno: String
};

const atencionSchema = new mongoose.Schema({

    id_caso: mongoose.Types.ObjectId,
    id_colaborador: mongoose.Types.ObjectId,
    colaborador: {
        type: ColaboradorSubSchema
    },
    fecha_creacion: { type: Date, default: Date.now },
    id_cuestionario:mongoose.Types.ObjectId,
    resultado_cuestionario:[Object],
    nota:{
        type:NotaSchema
    },
    medico: medicoSchema,
    tipo:{
        type:String,
        default:"DIARIA"
    },
    status:{
        type:Boolean,
        default:true
    }
})

atencionSchema.set('collection','atencion');
const Atencion = mongoose.model('atencion',atencionSchema);
exports.Atencion = Atencion;