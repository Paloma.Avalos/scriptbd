const mongoose = require('mongoose');

const { ColaboradorSubSchema } = require('./subDocument/colaborador');
const {NotaSchema} = require('./nota');
var Schema = mongoose.Schema;


const medicoSchema = {
    id_medico: {type: Schema.ObjectId, ref: "usuario"},
    nombre: String,
    apellido_paterno: String,
    apellido_materno: String
};

const situacionScema= new mongoose.Schema({
    id_caso:mongoose.Types.ObjectId,
    id_colaborador: mongoose.Types.ObjectId,
    colaborador:{
        type:ColaboradorSubSchema
    },
    medico: medicoSchema,
    situacion:String,
    nota:{
        type:NotaSchema
    },
    fecha_creacion:{
        type:Date,
        default:Date.now
    },
    status:{
        type:Boolean,
        default:true
    }

})


situacionScema.set('collection','situacion_colaborador');
const Situacion = mongoose.model('situacion_colaborador', situacionScema);
exports.Situacion = Situacion;
