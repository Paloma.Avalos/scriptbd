const mongoose = require('mongoose');
var mongoosePaginate = require("mongoose-paginate-v2");
var aggregatePaginate = require("mongoose-aggregate-paginate-v2");
var Schema = mongoose.Schema;
const { ColaboradorSubSchema } = require('./subDocument/colaborador');


const medicoSchema = {
    id_medico: { type: Schema.ObjectId, ref: "usuario" },
    nombre: String,
    apellido_paterno: String,
    apellido_materno: String
};

const pruebaSchema = new mongoose.Schema({

    id_tipo_prueba: {
        type: String
    },
    tipo_prueba: {
        type: String
    },
    id_tipo_carga_prueba: {
        type: String
    },
    tipo_carga_prueba: {
        type: String
    },
    medico: medicoSchema,
    fecha_programacion: {
        type: Date
    },
    fecha_realizacion: {
        type: Date
    },
    fecha_resultado: {
        type: Date
    },
    laboratorio: {
        type: String
    },
    id_resultado: {
        type: String
    },
    status:{
        type:Boolean,
        default:true
    },
    resultado: {
        type: String
    },
    fecha_creacion: {
        type: Date
    },
    url_prueba:{
        type:String
    },
    id_colaborador: mongoose.Types.ObjectId,
    id_caso: mongoose.Types.ObjectId,
    colaborador: {
        type: ColaboradorSubSchema
            // required: true
    }

});

pruebaSchema.plugin(mongoosePaginate); //second step
pruebaSchema.plugin(aggregatePaginate); //second step


pruebaSchema.set('collection', 'prueba');
const Prueba = mongoose.model('prueba', pruebaSchema);

exports.Prueba = Prueba;