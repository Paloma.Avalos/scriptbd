const mongoose = require('mongoose');

const { ColaboradorSubSchema } = require('./subDocument/colaborador')
const hospitalizacionSchema = new mongoose.Schema({

    motivo: {
        type: String
    },
    centro_medico: {
        type: String
    },
    fecha_ingreso: {
        type: Date
    },
    fecha_alta: {
        type: Date
    },
    retorno_labores: {
        type: Date
    },
    fecha_creacion: {
        type: Date
    },
    id_colaborador: mongoose.Types.ObjectId,
    id_caso: mongoose.Types.ObjectId,
    colaborador: {
        type: ColaboradorSubSchema
            // required: true
    }

});

hospitalizacionSchema.set('collection', 'hospitalizacion');
const Hospitalizacion = mongoose.model('hospitalizacion', hospitalizacionSchema);

exports.Hospitalizacion = Hospitalizacion;