const mongoose = require('mongoose');
var mongoosePaginate = require("mongoose-paginate-v2");
var aggregatePaginate = require("mongoose-aggregate-paginate-v2");
const {firmaSubSchema} = require('./subDocument/firma');
var Schema = mongoose.Schema;

const colaboradorSchema = new mongoose.Schema({
    apellido_paterno: {
        type: String,
        required: true
    },
    apellido_materno: {
        type: String,
        required: true
    },
    nombres: {
        type: String,
        required: true
    },
    nombre_completo: {
        type: String,
        default: function() {
            return this.nombres.trim() + ' ' + this.apellido_paterno.trim() + ' ' + this.apellido_materno.trim()
        }
    },
    seguro_atencion_medica: {
        type: String
    },
    id_tipo_documento: Number,
    tipo_documento: {
        type: String,
        required: true
    },
    numero_documento: {
        type: String,
        required: true,
        unique: true
    },
    fecha_nacimiento: Date,
    telefono: {
        type: String
    },
    correo_electronico: {
        type: String,
        required: true,
        unique: true
    },
    cargo: String,
    jefe_directo: String,
    datos_compania: {
        id_pais: Number,
        cod_pais: String,
        pais: String,
        id_grupo: Number,
        nombre_grupo: String,
        compania: String,
        id_area: {
            type: Number,
            required: true
        },
        area: {
            type: String,
            required: true
        },
        id_unidad: {
            type: Number,
            required: true
        },
        unidad: {
            type: String,
            required: true
        },
        id_sede: {
            type: Number,
            required: true
        },
        sede: {
            type: String,
            required: true
        },
        id_centro_costo: Number,
        centro_costo: String,
        subarea: String
    },
    id_riesgo_exposicion: Number,
    riesgo_exposicion: String,
    activo: Boolean,
    triaje_inicial_completado:{
        type: Boolean, default: true
    },
    resultado_sintomatologico: {
        llave: {
            type: String
        },
        valor: {
            type: String
        },
        orden: {
            type: Number
        }      
    },
    fecha_creacion: { type: Date, default: Date.now },
   /* nombre_completo: {
        type: String
    },*/
    sexo: String,
    id_tipo_colaborador: {
        type: Number,
        required: true
    },
    tipo_colaborador: {
        type: String,
        required: true
    },
    datos_usuario: {
        nombre_usuario: String,
        password: String,
        roles: String
    },
    ubigeo: Object,
    direccion: String,
    datos_contacto: Array,
    pre_existencias: Array,
    grupos_riesgo: Array,
    numero_personas_vive: Number,
    resultado_medico: [{
        condiciones: String,
        id_condicion: String,
        nota: String,
        fecha_registro: {
            type: Date,
            default: Date.now
        },
        id_medico: String,
        nombre_medico: String
    }],
    factores_riesgo: Array,
    id_device: String,
    fecha_creacion_device: Date,
    resultado_evaluacion: Object,
    resultado_sintomatologico: Object,
    numero_sintomas: Number,
    fecha_ultimo_test: Date,
    registro_firma:{
        type:Boolean,
        default:false
    },
    firma:{
        type:firmaSubSchema
    },
    id_test_inicial: { type: Schema.ObjectId, ref: "test" },
    id_test_diario: { type: Schema.ObjectId, ref: "test" }

});

colaboradorSchema.plugin(mongoosePaginate); //second step
colaboradorSchema.plugin(aggregatePaginate); //second step


colaboradorSchema.index({
    numero_documento: 1,
    correo_electronico: 1
}, {
    unique: true
});

colaboradorSchema.set('collection', 'colaborador');
const User = mongoose.model('usuario', colaboradorSchema);

exports.User = User;