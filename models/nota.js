const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const medicoSchema = {
    id_medico: {type: Schema.ObjectId, ref: "usuario"},
    nombre: String,
    apellido_paterno: String,
    apellido_materno: String
};

const notaSchema = new mongoose.Schema({
    id_colaborador: {type: Schema.ObjectId, ref: "usuario"},
    id_caso:{type:Schema.ObjectId},
    observacion: String,
    medico: medicoSchema,
    fecha_creacion: {
        type:Date,
        default:Date.now
    },
    fecha_actualizacion: Date,
    tipo:{
        type:String,
        default:"default"
    },
    activo: {type: Boolean, default: true}

});

notaSchema.set('collection', 'nota');
const Nota = mongoose.model('nota', notaSchema);

exports.Nota = Nota;
exports.NotaSchema=notaSchema;