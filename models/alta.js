const mongoose = require('mongoose');
const { ColaboradorSubSchema } = require('./subDocument/colaborador');
//const { ColaCasoSubSchema} = require('../models/caso');
var Schema = mongoose.Schema;
const { TriajeSubSchema } = require('./subDocument/triaje');

const medicoSchema = {
    id_medico: {type: Schema.ObjectId, ref: "usuario"},
    nombre: String,
    apellido_paterno: String,
    apellido_materno: String
};

const casoSubSchema = {
    id_colaborador: {type: Schema.ObjectId, ref: "usuario"},
    caso_finalizado: Boolean,
    caso_tipo: String
};

const altaSchema = new mongoose.Schema({


    id_caso: mongoose.Types.ObjectId,
    id_colaborador: mongoose.Types.ObjectId,
    colaborador: {
        type: ColaboradorSubSchema
    },
    caso:casoSubSchema,
    // {
    //     type: ColaCasoSubSchema
    // },
    medico: medicoSchema,
    fecha_inicio:Date,
    fecha_alta:Date,
    tipo:String,
    fecha_creacion:{
        type:Date,
        default:Date.now
    },
    status:Boolean,
    test: {
        type: TriajeSubSchema
            // required: true
    }
})

altaSchema.set('collection','alta');
const Alta = mongoose.model('alta',altaSchema);
exports.Alta = Alta;