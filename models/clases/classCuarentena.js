class cuarentenaSchema {

    constructor(tipo_documento, numero_documento, motivo, fecha_ingreso, fecha_alta, retorno_labores, fecha_creacion) {
        this.tipo_documento = tipo_documento;
        this.numero_documento = numero_documento;
        this.motivo = motivo;
        this.fecha_ingreso = fecha_ingreso;
        this.fecha_alta = fecha_alta;
        this.retorno_labores = retorno_labores;
        this.fecha_creacion = fecha_creacion;

    }

}

exports.ClassCuarentena = cuarentenaSchema;