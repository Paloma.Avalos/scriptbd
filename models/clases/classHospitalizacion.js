class hospitalizacionSchema {

    constructor(motivo, centro_medico, fecha_ingreso, fecha_alta, retorno_labores, fecha_creacion) {
        this.motivo = motivo;
        this.centro_medico = centro_medico;
        this.fecha_ingreso = fecha_ingreso;
        this.fecha_alta = fecha_alta;
        this.retorno_labores = retorno_labores;
        this.fecha_creacion = fecha_creacion;

    }
}

exports.ClassHospitalizacion = hospitalizacionSchema;