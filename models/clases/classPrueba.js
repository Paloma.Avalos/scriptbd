class PruebaSchema {

    constructor(tipo_documento, numero_documento, id_tipo_prueba, tipo_prueba,id_tipo_carga_prueba,tipo_carga_prueba, fecha_programacion, fecha_realizacion, fecha_resultado, laboratorio, id_resultado, resultado, fecha_creacion) {
        this.tipo_documento = tipo_documento;
        this.numero_documento = numero_documento;
        this.id_tipo_prueba = id_tipo_prueba;
        this.tipo_prueba = tipo_prueba;
        this.id_tipo_carga_prueba = id_tipo_carga_prueba;
        this.tipo_carga_prueba = tipo_carga_prueba;
        this.fecha_programacion = fecha_programacion;
        this.fecha_realizacion = fecha_realizacion;
        this.fecha_resultado = fecha_resultado;
        this.laboratorio = laboratorio;
        this.id_resultado = id_resultado;
        this.resultado = resultado;
        this.fecha_creacion = fecha_creacion;
    }

}


exports.ClassPrueba = PruebaSchema;