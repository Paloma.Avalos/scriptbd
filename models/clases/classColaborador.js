class ColaboradorSchema {

    constructor(apellido_paterno, apellido_materno, nombres, id_tipo_documento, numero_documento, telefono, correo_electronico, datos_compania, factores_riesgo,activo) {
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        this.nombres = nombres;
        this.id_tipo_documento = id_tipo_documento;
        this.numero_documento = numero_documento;
        this.telefono = telefono;
        this.correo_electronico = correo_electronico;
        this.datos_compania = datos_compania;
        this.factores_riesgo = factores_riesgo;
        this.activo = activo;
    }



}


exports.ClassColaborador = ColaboradorSchema;