const mongoose = require('mongoose');

const { ColaboradorSubSchema } = require('./subDocument/colaborador')
const cuarentenaSchema = new mongoose.Schema({

    motivo: {
        type: String
    },
    fecha_ingreso: {
        type: Date
    },
    fecha_alta: {
        type: Date
    },
    retorno_labores: {
        type: Date
    },
    fecha_creacion: {
        type: Date
    },
    id_colaborador: mongoose.Types.ObjectId,
    id_caso: mongoose.Types.ObjectId,
    colaborador: {
        type: ColaboradorSubSchema
            // required: true
    }

});

cuarentenaSchema.set('collection', 'cuarentena');
const Cuarentena = mongoose.model('cuarentena', cuarentenaSchema);

exports.Cuarentena = Cuarentena;