const mongoose = require('mongoose');

const configuracionSchema = new mongoose.Schema({
    activo: Boolean,
    compania_id: String,
    compania_nombre: String,
    host_functions: String,
    estilo: Object,
    host_webdashboard: String,
    metabase_secret_key: String,
    sintomatologia: Object,
    triaje_diario: Object,
    contact_tracing: Object,
    email_error: String,
    email_success: String
});

configuracionSchema.set('collection', 'configuracion');
const Configuracion = mongoose.model('configuracion', configuracionSchema);

exports.Configuracion = Configuracion;