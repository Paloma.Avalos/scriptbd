const mongoose = require('mongoose');



const alertaSchema = new mongoose.Schema({

    nombre: String, 
    email:String, 
    id_notificacion:Number,
    notificacion:String,
    status:{
        type:Boolean,
        default:true
    },
    fecha_creacion:{
        type:Date,
        default:Date.now
    }

})

alertaSchema.set('collection','alerta');
const Alerta = mongoose.model('alerta', alertaSchema, 'alerta');


exports.Alerta = Alerta;