const { Configuracion } = require('../../models/configuracion.js');
const { ResourceNotFoundError, InternalError } = require("../../error/BusinessError");

class ConfiguracionDb {

    static async obtenerMetabaseKey() {

        try {
            return await Configuracion.findOne().select('_id metabase_secret_key').lean();
        } catch (error) {
            throw new ResourceNotFoundError('No se encontro buscarConfiguracion');
        }
    }

}

module.exports.ConfiguracionDb = ConfiguracionDb;