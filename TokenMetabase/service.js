const _ = require('lodash');
const { ConfiguracionDb } = require('./db/ConfiguracionDb');
const { ResourceNotFoundError, InternalError } = require("../error/BusinessError");
const util = require('../util/util');
const constante = require('../util/constante');
const seguridad = require('../util/seguridad');

const serviceTokenMetabase = {

    obtenertokenMetabase: async function() {

        let format = 'YYYY-MM-DD';
        let jsonRpta = {};
        let fecha_desde = util.obtenerFechaUtc(util.obtenerFechaNowFormat(format));
        let fecha_hasta = util.sumarDias(fecha_desde, format, 1);
        let payload = {
            resource: { dashboard: constante.METABASE_DASHBOARD },
            params: {
                desde: fecha_desde,
                hasta: fecha_hasta
            },
            exp: Math.round(Date.now() / 1000) + (10 * 60) // 10 minute expiration
        };

        try {
            let key = await ConfiguracionDb.obtenerMetabaseKey();

            if (_.isEmpty(key.metabase_secret_key)) {
                throw new ResourceNotFoundError('El jwt metabase key no se ha encontrado');
            }

            console.log('payload:', payload);
            let tokenMetabase = seguridad.generarTokenMetabase(payload, key.metabase_secret_key);

            let iframeUrl = constante.METABASE_SITE_URL + "/embed/dashboard/" + tokenMetabase + "#bordered=true&titled=true";
            console.log('iframeUrl:', iframeUrl);

            jsonRpta.respuesta = 'Se ha creado la url para metabase';
            jsonRpta.iframeUrl = iframeUrl;

            return jsonRpta;
        } catch (e) {
            console.error(e);
            throw e;
        }

    }

};


module.exports = serviceTokenMetabase;