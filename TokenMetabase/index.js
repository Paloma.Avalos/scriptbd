//const validation = require('./validation/trabajador.validation');
const service = require('./service');

module.exports = async function(context, req) {


    let data = {
        _id: '',
        respuesta: 'Se obtiene el token para metabase'
    };

    let error = null;
    let response = null;

    try {
        let requestHandler = require('../handler/request-handler');
        await requestHandler.getRequestData(req, null, true, false);
        data = await service.obtenertokenMetabase();
    } catch (err) {
        error = err;
    } finally {
        response = require('../handler/response-handler').getResponse(data, error);
    }

    context.res = {
        status: response.status,
        body: response.body,
        headers: {
            'Content-Type': 'application/json'
        }
    };

};