const mongoose = require('mongoose');
const util = require('./util/util');
const { Test } = require('./models/test');
const _ = require('lodash');
const constante = require('./util/constante')


async function main() {
    console.log("Iniciando proceso de validación de caso");

    await mongoose.connect("mongodb+srv://healthcaredev:Admin123@clustercompanya-9vbux.azure.mongodb.net/healthcare-cte?authSource=admin&replicaSet=ClusterCompanyA-shard-0&readPreference=primary&ssl=true", {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });


    const listTestTodos = await Test.find();
    console.log("listTestTodos: " + listTestTodos.length);
    //console.log("listTestTodos: " + listTestTodos);
    for (let i = 0; i < listTestTodos.length; i++) {
        console.log("TEST: " + listTestTodos[i]["_id"]);

        let fechaCreacionInicialSinFormato = listTestTodos[i]["fecha_creacion"];
        let fechaCreacionConFormato = util.obtenerFechaPeru(listTestTodos[i]["fecha_creacion"]);
        
        console.log("fechaCreacionInicialSinFormato: " + fechaCreacionInicialSinFormato);
        console.log("fechaCreacionConFormato: " +  fechaCreacionConFormato);

        let UpdateFechaCreacion = util.obtenerFechaUtcFormat(util.obtenerFechaPeru( fechaCreacionConFormato));
        console.log("UpdateFechaCreacion: " + UpdateFechaCreacion);


        let updateTest = await Test.findByIdAndUpdate({ _id: listTestTodos[i]["_id"] }, { "$set": { fecha_creacion: fechaCreacionConFormato } })
        console.log("TEST actualizado: " + updateTest);
    }


}


main()
    .then(v => console.log("Se termino la carga de casos"))
    .catch(err => console.error(err));