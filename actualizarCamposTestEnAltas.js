const mongoose = require('mongoose');
const path = require('path');
const fs = require('fs');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const CSVToJSON = require('csvtojson');
const { Caso } = require('./models/caso');
const { Alta } = require('./models/alta');
const _ = require('lodash');
const constante = require('./util/constante')
const util = require('./util/util');

const test = {
    resultado_sintomatologico: Object,
    resultado_cuestionario: Array

};

const resultado_sintomatologico = {
    llave: String,
    valor: String
};

// const resultado_cuestionario = {
//     llave:String,
//     valor: String
// };

async function main() {

    console.log("Iniciando proceso de validación de caso");

    //CONEXION A BASE DATOS
    await mongoose.connect("mongodb+srv://healthcaredev:Admin123@clustercompanya-9vbux.azure.mongodb.net/healthcare-cte?authSource=admin&replicaSet=ClusterCompanyA-shard-0&readPreference=primary&ssl=true", {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    const CasoColaborador = await CSVToJSON().fromFile('./OutputCsv/colaboradorDNI.csv');
    console.log("Cantidad de CasosColaborador a actualizar: " + CasoColaborador.length);

    for (let i = 0; i < CasoColaborador.length; i++) {
        console.log("CasoColaborador - Posición : " + [i] + " - " + CasoColaborador[i]["numero_documento"]);

        //Validamos si existen caso con el Nro de documento del colaborador
        let getCaso = await Caso.find({ "colaborador.numero_documento": CasoColaborador[i]["numero_documento"] }).lean().sort({ "fecha_creacion": 'desc' });
        console.log("getCaso: " + getCaso);

        if (!util.esVacio(getCaso)) {
            console.log("Cantidad getCaso: " + getCaso.length);
            for (let i = 0; i < getCaso.length; i++) {

                if (!util.esVacio(getCaso[i].test)) {
                    let idCaso = getCaso[i]._id;
                    let llave = getCaso[i].test.resultado_sintomatologico.llave;
                    let valor = getCaso[i].test.resultado_sintomatologico.valor;

                    console.log("Caso - idCaso: " + idCaso);
                    console.log("Caso - llave: " + llave);
                    console.log("Caso - valor: " + valor);

                    let alta = await Alta.find({ "id_caso": getCaso[i]._id }).lean().sort({ "fecha_creacion": 'desc' });
                    console.log("Detalle Alta: " + alta);
                    
                    test.resultado_sintomatologico = getCaso[i].test.resultado_sintomatologico;
                    console.log("test.resultado_sintomatologico: " + test.resultado_sintomatologico);

                    let AltaUpdate = await Alta.findByIdAndUpdate(alta[`0`]._id, { "$set": { test: test } });


                }
            }
        }
    }
}


main()
    .then(v => console.log("Se termino la carga de casos"))
    .catch(err => console.error(err));