const mongoose = require('mongoose');
const path = require('path');
const fs = require('fs');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const CSVToJSON = require('csvtojson');
const { Prueba } = require('./models/prueba');
const _ = require('lodash');
const constante = require('./util/constante')


async function main() {
    console.log("Iniciando proceso de validación de caso");

    await mongoose.connect("mongodb+srv://healthcaredev:Admin123@clustercompanya-9vbux.azure.mongodb.net/healthcare-cte?authSource=admin&replicaSet=ClusterCompanyA-shard-0&readPreference=primary&ssl=true", {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    //Obtener todos los casos con el campo antiguo estado.
    const listPruebaTodos = await Prueba.find({ tipo_carga_prueba: "PROGRAMADA" });
    console.log("CANTIDAD DE PRUEBAS PROGRAMADAS ONTENIDAS" + listPruebaTodos.length);
    //Cambiamos el nombre del campo estado por status y lo actualizamos
    await Prueba.updateMany({ }, { "$set": { tipo_carga_prueba: "PENDIENTE" } });

    console.log("SE ACTUALIZO EL NOMBRE DEL CAMPO CORRECTAMENTE EN EL LISTADO DE PRUEBAS" + listPruebaTodos.length);
   


}

main()
    .then(v => console.log("Se termino la carga de casos"))
    .catch(err => console.error(err));