const mongoose = require('mongoose');
const path = require('path');
const fs = require('fs');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const CSVToJSON = require('csvtojson');
const { Caso } = require('./models/caso');
const _ = require('lodash');
const constante = require('./util/constante')


async function main() {
    console.log("Iniciando proceso de validación de caso");

    await mongoose.connect("mongodb+srv://healthcaredev:Admin123@clustercompanya-9vbux.azure.mongodb.net/healthcare-cte?authSource=admin&replicaSet=ClusterCompanyA-shard-0&readPreference=primary&ssl=true", {
        //"mongodb+srv://tempopodappuser:nDggmVkAKWdHjmXf@clustertempopd.lzf3m.azure.mongodb.net/healthcare", {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });


    const listCasoTodos = await Caso.find({ caso_tipo: "POSITIVO COVID" });
    console.log("listCaso POSITIVO COVID: " + listCasoTodos.length);

    await Caso.updateMany( {caso_tipo: "POSITIVO COVID"}, {"$set": { caso_tipo: "CONFIRMADO"} } );
    
    const listCasoActualizados = await Caso.find({ caso_tipo: "CONFIRMADO" });
    console.log("listCaso Actualizados a CONFIRMADO: " + listCasoActualizados.length);

}


main()
    .then(v => console.log("Se termino la carga de casos"))
    .catch(err => console.error(err));